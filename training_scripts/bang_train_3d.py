#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
     BANGalign : pseudo Bond and torsion ANGle aligner
     Copyright (C) 2009  Sebastian Kelm
 
     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from __future__ import print_function

import sys
import os
import itertools
import random

from prosci.common import parallelise
from prosci.util.picklejar import PickleJar
from prosci.util.bangalign import bangalign_file_pairs


def indices(seq1, seq2):
  """Converts two aligned sequences into two lists of residue indices.

  The length of ali1 is the same as the ungapped length of the protein, and
  contains the indices in the ungapped seq2 that each seq1 residue is aligned
  to (-1 for gaps).
  """
  assert len(seq1) == len(seq2)
  ali1 = []
  ali2 = []
  n1=0
  n2=0
  for i,c1 in enumerate(seq1):
    c2 = seq2[i]
    if c1 != '-' and c2 != '-':
      ali1.append(n2)
      ali2.append(n1)
      n1+=1
      n2+=1
    elif c1 != '-':
      ali1.append(-1)
      n1+=1
    elif c2 != '-':
      ali2.append(-1)
      n2+=1
  assert len(seq1)-seq1.count("-") == len(ali1)
  assert len(seq2)-seq2.count("-") == len(ali2)
  return ali1, ali2

def score(q, m, cq, cm):
  """Score the alignment against a reference alignment.

  Takes lists of residue indices as input, as produced by indices().
  """
  assert len(q) == len(cq), "%d  %d\n%s\n%s\n" % (len(q), len(cq), str(q), str(cq))
  assert len(m) == len(cm), "%d  %d\n%s\n%s\n" % (len(m), len(cm), str(m), str(cm))
  s=0
  total=0
  for i,v in enumerate(q):
    if cq[i] >= 0: # Only account for residues that TMalign has aligned to something
      if v == cq[i]: # Aligned to the same residue on the other protein
        s+=1
      total += 1
  for i,v in enumerate(m):
    if cm[i] >= 0: # Only account for residues that TMalign has aligned to something
      if v == cm[i]: # Aligned to the same residue on the other protein
        s+=1
      total += 1
  return float(s)/total

def score2(q, m, cq, cm, res_dist_cutoff=15):
  """Score the alignment against a reference alignment.

  Takes lists of residue indices as input, as produced by indices().
  """
  #assert len(q.replace("-", "")) == len(cq.replace("-", "")), "%d  %d\n%s\n%s\n" % (len(q), len(cq), str(q), str(cq))
  #assert len(m.replace("-", "")) == len(cm.replace("-", "")), "%d  %d\n%s\n%s\n" % (len(m), len(cm), str(m), str(cm))
  #q, m = indices(q, m)
  #cq, cm = indices(cq, cm)
  assert len(q) == len(cq), "%d  %d\n%s\n%s\n" % (len(q), len(cq), str(q), str(cq))
  assert len(m) == len(cm), "%d  %d\n%s\n%s\n" % (len(m), len(cm), str(m), str(cm))

  s=0
  total=0
  for i,v in enumerate(q):
    if cq[i] >= 0: # Only account for residues that TMalign has aligned to something
      if v >= 0 and abs(v - cq[i]) <= res_dist_cutoff: # Aligned to the same residue on the other protein
        s += float(res_dist_cutoff-abs(v - cq[i])) / res_dist_cutoff
      total += 1
  for i,v in enumerate(m):
    if cm[i] >= 0: # Only account for residues that TMalign has aligned to something
      if v >= 0 and abs(v - cm[i]) <= res_dist_cutoff: # Aligned to the same residue on the other protein
        s += float(res_dist_cutoff-abs(v - cm[i])) / res_dist_cutoff
      total += 1
  return float(s)/total


def make_training_subset(training_table_fname, ntrain=100, bufferfile="reduced_training_table.pickle"):
    MINTM=0.6
    reduced_training_table = PickleJar(bufferfile)
    if len(reduced_training_table) < ntrain:
      reduced_training_table.clear()
      training_table = PickleJar(training_table_fname)
      keys = list(training_table.keys())
      random.shuffle(keys)
      histogram = []
      for i in range(5):
        histogram.append([])
      for k in keys:
        pdbf1, pdbf2 = k
        #if pdbf1 == pdbf2: continue
        v = training_table[k]
        tm = max(v[0], v[1])
        if tm < MINTM: continue
        i_hist = int((tm-MINTM)*10)
        assert i_hist >= 0
        histogram[i_hist].append(k)
      while len(reduced_training_table) < ntrain and sum([len(tmbin) for tmbin in histogram]):
        for tmbin in histogram:
          if not tmbin: continue
          k = tmbin.pop()
          reduced_training_table[k] = training_table[k]
      reduced_training_table.save()
    return reduced_training_table


def split_list(N, lst):
  pieces = []
  for i in range(N):
    pieces.append([])
  for i, v in enumerate(lst):
    pieces[i % N].append(v)
  return pieces


def train(all_combos, training_results):
  output = {}
  total_proteins = 0
  for i, opt in enumerate(all_combos):
    if opt in training_results:
      total_tm, total_score = training_results[opt]
    else:
    #if True:
      alignments = bangalign_file_pairs(struc_pairs, opt+" -W")
      total_score = 0.0
      total_tm = 0.0
      total_proteins = 0
      #print(alignments)
      for (pdbf1, pdbf2), (query, match, info), (correct_query, correct_match) in zip(struc_pairs, alignments, correct_alignments):
        if len(query) < 4:
          print(pdbf1, pdbf2, query, ";;;;;;;", match, file=sys.stderr)
          print("", file=sys.stderr)
        query, match = indices(query, match)
        total_score += score2(query, match, correct_query, correct_match)
        try:
          total_tm += info["TM-score"]
        except KeyError:
          pass
        total_proteins += 1
      total_score /= total_proteins
      total_tm /= total_proteins
    output[opt] = (total_tm, total_score)
    print("%5d/%d : %70s : %.4f %.4f"%(i+1, len(all_combos), opt, total_score, total_tm))
  return output



if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Train the parameters for bangalign')
    parser.add_argument('training_table', type=str, help='PickleJar file containing the input data')
    parser.add_argument('training_results', type=str, help='PickleJar file for saving the training results')
    args = parser.parse_args()
    
    reduced_table_fname, ext = os.path.splitext(args.training_table)
    reduced_table_fname += ".reduced" + ext
    
    # Subsample the rather large training set, for speed
    #
    training_table = make_training_subset(args.training_table, 100, reduced_table_fname)
    #for k,v in sorted(training_table.items(), key=lambda x: -max(x[1][:2])):
    #  print(max(v[:2]), k[0], k[1])
    print(len(training_table), "pairs of structures in training set")

    # Buffer training results in a pickle file
    #
    training_results = PickleJar(args.training_results)
    print(len(training_results), "buffered training results")

    # Prepare input proteins to be aligned and the correct alignment from TMalign
    #
    struc_pairs = []
    correct_alignments = []
    target_average_tm = 0.0
    for (pdbf1, pdbf2), (tm1, tm2, correct_query, correct_match) in training_table.items():
      if "X" in correct_query or "X" in correct_match: continue
      struc_pairs.append((pdbf1, pdbf2))
      correct_alignments.append(indices(correct_query, correct_match))
      target_average_tm += max(tm1, tm2)
    target_average_tm /= float(len(struc_pairs))
    del training_table


    #                                        3d gap opening & extension
    #all_combos = [x for x in itertools.product([0.01*x for x in range(10, 1001, 10)], [0.01*x for x in range(1,200,10)]) if x[0]>x[1]]
    #opt_format = "-n -%.4f -d -%.4f"
    
    #                                        initial alignment parameters
    #all_combos = list(itertools.product([9], [11.4], [2.0], [26], [0.7], [7], [21], [0.039]))
    #all_combos = list(itertools.product([9], [0.1*x for x in (range(50, 150))], [2.0], [26], [0.1*x for x in (range(1, 50, 3))], [7], [21], [0.001*x for x in (range(0, 50, 3))]))
    #all_combos = list(itertools.product([9], [0.1*x for x in (range(50, 150))], [2.0], [26], [0], [7], [21], [0.001*x for x in (range(0, 1000, 50))]))
    #all_combos = list(itertools.product([9], [0.1*x for x in (range(50, 150))], [2.0], [26], [0], [7], [21], [0]))        ### <<<
    #all_combos = list(itertools.product([9], [0.1*x for x in (range(90, 110))], [2.0], [26], [0], [7], [21], [2.5]))
    #all_combos = list(itertools.product([11], [10], [2.0], range(31), [0.1*x for x in (range(0, 21))], [6], [17], [0.7]))
    #opt_format = "-w %d -o -%.4f -e -%.4f -x %d -u %.4f -y %d -a %d -z %.4f -n -3.5000 -d -0.3100"
    #all_combos = [x for x in itertools.product([11], [10], [2.0], [0], [0.0], [6], [17], [0.7], [0.01*x for x in range(150, 551, 10)], [0.01*x for x in range(0,71,10)]) if x[-2] > x[-1]]
    #all_combos = [x for x in itertools.product([11], [10], [2.0], [0], [0.0], [6], [17], [0.7], [0.01*x for x in range(250, 301, 10)], [0.01*x for x in range(0,51,10)]) if x[-2] > x[-1]]
    #all_combos = [x for x in itertools.product([11], [10], [2.0], [0], [0.0], [6], [17], [0.7], [0.01*x for x in range(50, 301, 10)], [0]) if x[-2] > x[-1]]
    #all_combos = [x for x in itertools.product([11], [10], [2.0], [0], [0.0], [6], [17], [0.7], [0.01*x for x in range(50, 301, 10)], [0.01*x for x in range(0, 301, 10)]) if x[-2]/2 >= x[-1]]
    #all_combos = [x for x in itertools.product([11], [10], [2.0], [0], [0.0], [6], [17], [0.7], [1.1], [0.3])]
    #all_combos = [x for x in itertools.product([11], [0.1*x for x in (range(0, 150, 10))], [0.1*x for x in (range(0, 31, 10))], [0], [0.0], [6], [17], [0.7], [1.1], [0.3])]
    #all_combos = [x for x in itertools.product([11], [3], [0], [0], [0.0], [6], [17], [0.7], [0.01*x for x in range(50, 301, 10)], [0.01*x for x in range(0, 301, 10)]) if x[-2]/2 >= x[-1]]
    #all_combos = [x for x in itertools.product([11,9], [3,10], [0,2], [0], [0.0], [6], [17], [0.7], [1.5,1.1], [0.3]) if x[-2]/2 >= x[-1]]
    #all_combos = [x for x in itertools.product(range(6,12), [3], [0], [0], [0.0], range(6,12), [17], [0.7], [1.5], [0.3]) if x[-2]/2 >= x[-1]]
    #all_combos = [x for x in itertools.product([9], [3], [0], [0], [0.0], [6], range(6,20), [0.7], [1.5], [0.3]) if x[-2]/2 >= x[-1]]
    all_combos = [x for x in itertools.product([9], [3], [0], [0], [0.0], [6], [17], [0.7], [1.5], [0.3]) if x[-2]/2 >= x[-1]]
    #all_combos = [x for x in itertools.product([9], [3], [0], [0], [0.0], [6], [17], [0.7], [0.01*x for x in range(50, 301, 10)], [0.01*x for x in range(0, 301, 10)]) if x[-2]/2 >= x[-1]]
    opt_format = "-w %d -o -%.4f -e -%.4f -x %d -u %.4f -y %d -a %d -z %.4f -n -%.4f -d -%.4f"

    #all_combos = list(itertools.product([9], [11.4], [2.0], [26], [0.7], [7], [21], [0.039]))
    #opt_format = "-w %d -o -%.4f -e -%.4f -x %d -u %.4f -y %d -a %d -z %.4f"

    
    all_combos = [opt_format % tuple(options) for options in all_combos]
    print(len(all_combos), "possible parameter sets")
    random.shuffle(all_combos)
    
    CPUS = 4
    NCHECKPOINT = 10
    this_run_only = {}
    while True:
      
      if this_run_only:
        print()
        print("This run only - current ranking:")
        ranking = sorted(this_run_only.items(), key=(lambda x:x[1]), reverse=True)
        for i, (opt, (total_tm, total_score)) in enumerate(ranking[:20]):
          print("%2d : %70s : %.4f %.4f"%(i+1, opt, total_score, total_tm))
        print()
        print(len(all_combos), "combinations left to try")
      
      if training_results:
        print()
        print("All time best - ranking:")
        ranking = sorted(training_results.items(), key=(lambda x:x[1]), reverse=True)
        for i, (opt, (total_tm, total_score)) in enumerate(ranking[:20]):
          print("%2d : %70s : %.4f %.4f"%(i+1, opt, total_score, total_tm))
      
      print()
      print("--------------------------------------------------")
      print("Target average TM over entire training set: %.4f"%(target_average_tm))
      print("--------------------------------------------------")
      
      if not all_combos: break
    
      combos = all_combos[-NCHECKPOINT*CPUS:]
      del all_combos[-NCHECKPOINT*CPUS:]
      tr = dict(training_results)
      sublists = split_list(CPUS, combos)
      results = parallelise(CPUS, [(train, (args, tr)) for args in sublists])
      for r in results:
        this_run_only.update(r)
      training_results.update(this_run_only)
      training_results.save()
    
    print()
    print("Done.\n", file=sys.stderr)
