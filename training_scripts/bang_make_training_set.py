#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import print_function

import sys
import os
import shlex
import subprocess

from prosci.util.picklejar import PickleJar


def make_training_set(pdbs, picklename="bang_training_data.pickle"):
  pdbs = sorted(pdbs)
  if isinstance(picklename, PickleJar):
    scoretable = picklename
  else:
    scoretable = PickleJar(picklename)
  
  for k in list(scoretable.keys()):
    if " " in k:
      scoretable[tuple(k.split(" ", 1))] = scoretable[k]
      del scoretable[k]
  
  try:
    for i, p1 in enumerate(pdbs):
      for j in range(i+1, len(pdbs)):
        p2 = pdbs[j]
        key = (p1, p2)
        if key in scoretable:
          continue
        p = subprocess.Popen(shlex.split("TMalign %s %s" % (p1, p2)), stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        out, err = p.communicate()
        if err:
          print("ERROR:", key, file=sys.stderr)
          print(err, file=sys.stderr)
          continue
        out = out.strip().splitlines()
        tm_scores = []
        for k, line in enumerate(out):
            if line.startswith("TM-score="):
              tm_scores.append(float(line.split("=")[1].split()[0]))
            elif line.startswith('(":"'):
              seq1=out[k+1].strip()
              seq2=out[k+3].strip()
              break
        scoretable[key] = (tm_scores[0], tm_scores[1], seq1, seq2)
        if (i * len(pdbs) + j) % 50 == 0:
          print(key, tm_scores[0], tm_scores[1], seq1, seq2)
  finally:
    scoretable.save()
  return scoretable


def convert_training_set(old_fname, picklename="bang_training_data.pickle"):
  if isinstance(picklename, PickleJar):
    scoretable = picklename
  else:
    scoretable = PickleJar(picklename)
  try:
    with open(old_fname) as f:
      for line in f:
        try:
          p1, p2, tm1, tm2, seq1, seq2 = line.split()
        except ValueError as e:
          #print("Error reading:", line.rstrip(), file=sys.stderr)
          continue
        if p2 < p1:
          p1, p2 = p2, p1
          tm1, tm2 = tm2, tm1
          seq1, seq1 = seq2, seq1
        key = (p1, p2)
        if key in scoretable:
          continue
        scoretable[key] = (float(tm1), float(tm2), seq1, seq2)
  finally:
    scoretable.save()
  return scoretable


def filesize(fname):
  return os.stat(fname).st_size


if __name__ == "__main__":
  print("Converting old training data...")
  convert_training_set("../TM_matrix.txt", "../bang_training_data.pickle")
  
  pdbs = sys.argv[1:]
  if not pdbs: sys.exit()
  pdbs = [x for x in pdbs if filesize(x) > 1]
  
  print("Making additional training data...")
  make_training_set(pdbs, "../bang_training_data.pickle")
