/*
     BANGalign : pseudo Bond and torsion ANGle aligner
     Copyright (C) 2009  Sebastian Kelm
 
     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "skutil.h"
#include "pdb.h"

#define P(EX) cout << #EX << ": " << EX << endl;


using namespace std;


Pdb::Pdb(char* n)
: name(n)
{
  ifstream in(n);
  initAtoms(in);
}
Pdb::Pdb(const string n)
: name(n)
{
  ifstream in(name.c_str());
  initAtoms(in);
}
Pdb::Pdb(const string n, istream& in)
: name(n)
{
  initAtoms(in);
}
Pdb::Pdb(const string n, vector<Atom>& a, int resCount)
: name(n),
  atoms(a),
  residueCount(resCount)
{
}
Pdb::Pdb(const Pdb &pdb)
: name(pdb.name),
  atoms(pdb.atoms),
  residueCount(pdb.residueCount)
{
}
Pdb::Pdb(const char* n, const vector<string>* atomTypes)
: name(n)
{
  ifstream in(n);
  initAtoms(in, atomTypes);
}
Pdb::Pdb(const string& n, const vector<string>* atomTypes)
: name(n)
{
  ifstream in(name.c_str());
  initAtoms(in, atomTypes);
}
Pdb::Pdb(const string& n, istream& in, const vector<string>* atomTypes)
: name(n)
{
  initAtoms(in, atomTypes);
}
Pdb::Pdb(const Pdb &pdb, const vector<string>* atomTypes)
: name(pdb.name)
{
  initAtoms(pdb.atoms, atomTypes);
}


Atom& Pdb::operator[](int x)
{
  require(x >= 0 && (unsigned)x < atoms.size(),
    "Pdb::operator[] out of range");
  return atoms[x];
}

const Atom& Pdb::operator[](int x) const
{
  require(x >= 0 && (unsigned)x < atoms.size(),
    "Pdb::operator[] out of range");
  return atoms[x];
}

void Pdb::initAtoms(istream& in)
{
    require(in, string("Could not read file ") + name + "\n");
    
    residueCount=0;
    
    // read file
    string line;
    int lastres=0;
    string lastinscode = "";
    while(getline(in, line))
    {
        if (line.substr(0,6) == "ATOM  " || line.substr(0,6) == "HETATM")
        {
//             cerr << "Building Atom for line: '" << line << "'\n";
            try
            {
                Atom atm(line);
                if (residues.toLetter(atm.res) == 'X')
                {
                    continue;
                }
                if ((atm.element != "H") && (atm.element != ""))
                {
                    if (atm.altloc > "A")
                    {
                        // Skipping alternative location
                        continue;
                    }
                    // add ATOM to vector of atoms for the current file
                    if (lastres != atm.ires || lastinscode != atm.inscode)
                    {
                      lastres = atm.ires;
                      lastinscode = atm.inscode;
                      atm.resindex = residueCount++;
                    }
                    else
                    {
                        atm.resindex = residueCount - 1;
                    }
                    atoms.push_back(atm);
                }
                else
                {
                    //cerr << "Skipping atom : \n\t";
                    //atm.print(cerr);
                }
            }
            catch (const BadConversion e)
            {
              cerr << "Warning: failed to parse atom record:" << line << endl;
            }
        }
    }
}

void Pdb::initAtoms(istream& in, const vector<string>* atype)
{
    require(in, string("Could not read file ") + name + "\n");
    
    residueCount=0;
    
    // read file
    string line;
    int lastres=0;
    string lastinscode = "";
    string allowed_altloc = "";
    while(getline(in, line))
    {
        if (line.substr(0,6) == "ATOM  " || line.substr(0,6) == "HETATM")
        {
//             cerr << "Building Atom for line: '" << line << "'\n";
            try
            {
                Atom atm(line);
                if (residues.toLetter(atm.res) == 'X')
                {
                    continue;
                }
                if ((atm.element != "H") && (atm.element != ""))
                {
                    if (atype != NULL and !contains(*atype, atm.atom))
                      continue;
                    // add ATOM to vector of atoms for the current file
                    if (lastres != atm.ires || lastinscode != atm.inscode)
                    {
                      lastres = atm.ires;
                      lastinscode = atm.inscode;
                      atm.resindex = residueCount++;
                      allowed_altloc = atm.altloc; // doesn't matter if atm.altloc is empty or not
                    }
                    else
                    {
                      if (atm.altloc != "")
                      {
                        if (allowed_altloc.size())
                        {
                          if (atm.altloc != allowed_altloc)
                            continue;
                        }
                        else
                        {
                          allowed_altloc = atm.altloc;
                        }
                      }
                      atm.resindex = residueCount - 1;
                    }
                    atoms.push_back(atm);
                }
                else
                {
                  #ifdef DEBUG
                    cerr << "Skipping atom : \n\t";
                    atm.print(cerr);
                  #endif
                }
            }
            catch (const BadConversion e)
            {
              cerr << "Warning: failed to parse atom record:" << line << endl;
            }
        }
    }
}

void Pdb::initAtoms(const vector<Atom>& newatoms, const vector<string>* atype)
{
    residueCount=0;
    
    // read file
    int lastres=0;
    string lastinscode = "";
    string allowed_altloc = "";
    for (size_t i=0; i<newatoms.size(); ++i)
    {
        Atom atm = newatoms[i];
        if (residues.toLetter(atm.res) == 'X')
        {
            continue;
        }
        if ((atm.element != "H") && (atm.element != ""))
        {
            if (atype != NULL and !contains(*atype, atm.atom))
              continue;
            // add ATOM to vector of atoms for the current file
            if (lastres != atm.ires || lastinscode != atm.inscode)
            {
              lastres = atm.ires;
              lastinscode = atm.inscode;
              atm.resindex = residueCount++;
              allowed_altloc = atm.altloc; // doesn't matter if atm.altloc is empty or not
            }
            else
            {
              if (atm.altloc != "")
              {
                if (allowed_altloc.size())
                {
                  if (atm.altloc != allowed_altloc)
                    continue;
                }
                else
                {
                  allowed_altloc = atm.altloc;
                }
              }
              atm.resindex = residueCount - 1;
            }
            atoms.push_back(atm);
        }
        else
        {
          #ifdef DEBUG
            cerr << "Skipping atom : \n\t";
            atm.print(cerr);
          #endif
        }
    }
}


void Pdb::add(Atom a)
{
    require(a.is_initialised(), "Pdb::add(Atom) : Atom is not initialised");
    
    if (size())
    {
      if (atoms.back().ires != a.ires || atoms.back().inscode != a.inscode)
      {
        a.resindex = residueCount++;
      }
      else
      {
        a.resindex = residueCount - 1;
      }
    }
    atoms.push_back(a);
}


Pdb Pdb::copyByAtomType(std::string atype) const
{
  vector<Atom> newAtoms;
  for(unsigned int i=0; i<atoms.size(); i++)
  {
    if (atype == atoms[i].atom)
    {
      newAtoms.push_back(atoms[i]);
    }
  }
  return Pdb(name, newAtoms, residueCount);
}

void Pdb::splitChains(std::vector<Pdb>& output, vector<string>& chainids) const
{
  require(output.size() == chainids.size(), "Pdb::splitChains : output vectors not of same size!");

  map< string, vector<Atom> > chains;
  for(unsigned int i=0; i<atoms.size(); i++)
  {
    string c = atoms[i].chain;
    chains[c].push_back(atoms[i]);
  }
  for(map< string, vector<Atom> >::iterator it=chains.begin(); it!=chains.end(); it++)
  {
    output.push_back(Pdb(name, it->second));
    chainids.push_back(it->first);
  }
}


void Pdb::sort()
{
  ::sort(atoms.begin(), atoms.end());
}


// Atom::Atom(Atom other)
// {
//     this->x=x;
//     this->y=y;
//     this->z=z;
//     this->iatom=iatom;
//     this->atom=atom;
//     this->ires=ires;
//     this->res=res;
//     this->chain=chain;
// }
// Atom::Atom(std::string x, std::string y, std::string z, std::string iatom, std::string atom, std::string ires, std::string res, std::string chain)
// {
//     this->x=convertTo<double>(x);
//     this->y=convertTo<double>(y);
//     this->z=convertTo<double>(z);
//     this->iatom=convertTo<int>(iatom);
//     this->atom=trim(atom);
//     this->ires=convertTo<int>(ires);
//     this->res=trim(res);
//     this->chain=trim(chain);
// }
Atom::Atom(const string& line)
{
    iatom = convertTo<int>(line.substr( 6,5));
    atom  = trim(line.substr(12,4));
    altloc  = trim(line.substr(16,1));
    res   = trim(line.substr(17,3));
    chain = trim(line.substr(21,1));
    ires  = convertTo<int>(line.substr(22,4));
    inscode = trim(line.substr(26,1));
    x = convertTo<double>(line.substr(30,8));
    y = convertTo<double>(line.substr(38,8));
    z = convertTo<double>(line.substr(46,8));
    try
    {
      occup = convertTo<double>(line.substr(54,6));
    }
    catch (const BadConversion e)
    {
      occup = 0;
    }
    catch (const std::out_of_range e)
    {
      occup = 0;
    }
    try
    {
      b = convertTo<double>(line.substr(60,6));
    }
    catch (const BadConversion e)
    {
      b = 0;
    }
    catch (const std::out_of_range e)
    {
      b = 0;
    }
    resindex = 0;
    
    // Detect element from atom identifier
    for (unsigned i=0; i<atom.length(); ++i)
    {
      char t = atom.at(i);
      if (!isdigit(t) && t != ' ')
      {
        element = t;
        break;
      }
    }
}

void Atom::print(ostream& out) const
{
    char buffer[100];
    
    // RAPDF format:
    //sprintf(buffer, "%8.3f %8.3f %8.3f %5d %-4s %s%4d%s %-3s %s\n", x, y, z, iatom, atom.c_str(), altloc.c_str(), ires, inscode.c_str(), res.c_str(), chain.c_str());
    
    sprintf(buffer, "ATOM  %5d %-4s%1s%-3s %1s%4d%1s   %8.3f%8.3f%8.3f%6.2f%6.2f\n", iatom, atom.c_str(), altloc.c_str(), res.c_str(), chain.c_str(), ires, inscode.c_str(), x, y, z, occup, b);
    
    // Python format:
    // "%s%5d %-4s%1s%-3s %1s%4d%1s   %8.3f%8.3f%8.3f%6.2f%6.2f          %2s%-2s\n" % (datatype, self.iatom, atom, self.altloc, self.res, self.chain, self.ires, self.inscode, self.x, self.y, self.z, self.occup, self.b, self.element, self.charge)
    out << buffer;
}

int Atom::operator<(const Atom& other) const
{
    if (ires != other.ires)
      return ires < other.ires;
    if (inscode != other.inscode)
      return inscode < other.inscode;
    if (altloc != other.altloc)
      return altloc < other.altloc;
    return atom < other.atom;
}

int Atom::operator>(const Atom& other) const
{
    if (ires != other.ires)
      return ires > other.ires;
    if (inscode != other.inscode)
      return inscode > other.inscode;
    if (altloc != other.altloc)
      return altloc > other.altloc;
    return atom > other.atom;
}

Atom& Atom::operator=(const Atom& other)
{
  x = other.x;
  y = other.y;
  z = other.z;
  occup = other.occup;
  b = other.b;
  iatom = other.iatom;
  ires = other.ires;
  resindex = other.resindex;
  atom = other.atom;
  res = other.res;
  chain = other.chain;
  altloc = other.altloc;
  inscode = other.inscode;
  return *this;
}

double atom_distance(const Atom& a, const Atom& b)
{
  return sqrt(pow((a.x - b.x), 2) + pow((a.y - b.y), 2) + pow((a.z - b.z), 2));
}


