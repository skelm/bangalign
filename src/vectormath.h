/*
     BANGalign : pseudo Bond and torsion ANGle aligner
     Copyright (C) 2009  Sebastian Kelm
 
     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VECTORMATH_H
#define VECTORMATH_H


#include <cmath>
#include <vector>

#include "skutil.h"
#include "pdb.h"


inline double square_distance(const Coord& a, const Coord& b)
{
  return pow((a.x - b.x), 2) + pow((a.y - b.y), 2) + pow((a.z - b.z), 2);
}

inline double distance(const Coord& a, const Coord& b)
{
  return sqrt(pow((a.x - b.x), 2) + pow((a.y - b.y), 2) + pow((a.z - b.z), 2));
}

inline void cross(const Coord& a, const Coord& b, Coord& c)
{
  c.assign(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}

inline double dot(const Coord& a, const Coord& b)
{
  return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline double norm(const Coord& a)
{
  return sqrt(dot(a, a));
}

inline void normalize(Coord& a)
{
  double  L = norm(a);
  a.x /= L;
  a.y /= L;
  a.z /= L;
}

inline double angle(const Coord& a, const Coord& b)
{
  return acos(dot(a, b)/(norm(a)*norm(b)));
}


inline void get_bond(const Coord& a, const Coord& b, Coord& out)
{
  out.assign(b.x - a.x, b.y - a.y, b.z - a.z);
}


inline std::vector<Coord> make_bonds(const Pdb& struc)
{
  std::vector<Coord> bonds(struc.size()-1);
  for (size_t i=1; i<struc.size(); i++)
    get_bond(struc[i-1], struc[i], bonds[i-1]);
  return bonds;
}


inline void make_angles(const std::vector<Coord> bonds, std::vector<double>& abond, std::vector<double>& adihedral)
{
  if (bonds.size() < 2)
    return;

  adihedral.push_back(0);
  for (unsigned i=1; i<bonds.size()-1; i++)
  {
    abond.push_back(angle(bonds[i-1], bonds[i]));
    adihedral.push_back(angle(bonds[i-1], bonds[i+1]));
  }
  abond.push_back(angle(bonds[bonds.size()-2], bonds[bonds.size()-1]));

  // The two angle vectors now have the same length
  // The first entry in the dihedrals is 0 and should not be used.
}

#endif
