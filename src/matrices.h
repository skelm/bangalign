/*
     BANGalign : pseudo Bond and torsion ANGle aligner
     Copyright (C) 2009  Sebastian Kelm
 
     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MATRICES_H
#define MATRICES_H

#include <vector>
#include <cmath>

#include "skutil.h"


template<typename T>
class Matrix3D
{
  size_t dimX, dimY, dimZ;
  std::vector<T> data;

  public:
    typedef typename std::vector<T>::iterator iterator;
    typedef typename std::vector<T>::const_iterator const_iterator;
  
    Matrix3D()
    : dimX(0), dimY(0), dimZ(0)
    {
    }
    
    Matrix3D(size_t x, size_t y, size_t z, T defVal)
    : dimX(x), dimY(y), dimZ(z), data(x*y*z, defVal)
    {
      if (x==0 or y==0 or z==0)
        throw OutOfBounds("Matrix3D::Matrix3D() : x,y,z dimensions must not be 0");
    }

    
    inline iterator begin() {return data.begin();}
    inline const_iterator begin() const {return data.begin();}
    
    inline iterator end() {return data.end();}
    inline const_iterator end() const {return data.end();}
    
    inline void fill(T value)
    {
      for (iterator it=begin(); it<end(); ++it)
        *it = value;
    }
    
    void add(const Matrix3D<T>& other, double factor)
    {
      require((other.X() == X()) and (other.Y() == Y()) and (other.Z() == Z()), "Matrix3D::add : other matrix must have same dimensions");
      
      iterator itA=begin();
      const_iterator itB=other.begin();
      for (; itA<end(); ++itA, ++itB)
      {
        *itA += *itB * factor;
      }
    }
    
    
    inline size_t size() const
    {
      return data.size();
    }
    inline size_t X() const
    {
      return dimX;
    }
    inline size_t Y() const
    {
      return dimY;
    }
    inline size_t Z() const
    {
      return dimZ;
    }
    
    inline T& operator()(size_t x, size_t y, size_t z)
    {
      if (z>=dimZ or y>=dimY or x>=dimX)
        throw OutOfBounds("Matrix3D::operator() : Index out of bounds "+toString(x)+","+toString(y)+","+toString(z)+". Bounds: "+toString(dimX)+","+toString(dimY)+","+toString(dimZ));
      return data[x + dimX*y + dimX*dimY*z];
    }
    inline const T& operator()(size_t x, size_t y, size_t z) const
    {
      if (z>=dimZ or y>=dimY or x>=dimX)
        throw OutOfBounds("Matrix3D::operator() : Index out of bounds "+toString(x)+","+toString(y)+","+toString(z)+". Bounds: "+toString(dimX)+","+toString(dimY)+","+toString(dimZ));
      return data[x + dimX*y + dimX*dimY*z];
    }
    
    inline T& operator[](size_t i)
    {
      if (i>=data.size())
        throw OutOfBounds("Matrix3D::operator[] : Index out of bounds "+toString(i)+". Bound: "+toString(data.size()));
      return data[i];
    }
    inline const T& operator[](size_t i) const
    {
      if (i>=data.size())
        throw OutOfBounds("Matrix3D::operator[] : Index out of bounds "+toString(i)+". Bound: "+toString(data.size()));
      return data[i];
    }
    
    inline void print(std::ostream& out = std::cout) const
    {
      out << toString(data);
    }
    
    inline T sum() const
    {
      T total = 0;
      for (const_iterator it=begin(); it<end(); ++it)
      {
        total += *it;
      }
      return total;
    }
    
};




template<typename T>
class Matrix2D
{
  size_t dimX, dimY;
  std::vector<T> data;

  public:
    typedef typename std::vector<T>::iterator iterator;
    typedef typename std::vector<T>::const_iterator const_iterator;
    
    
    Matrix2D()
    : dimX(0), dimY(0)
    {
    }
    
    Matrix2D(size_t x, size_t y, T defVal)
    : dimX(x), dimY(y), data(x*y, defVal)
    {
      if (x==0 or y==0)
        throw OutOfBounds("Matrix2D::Matrix2D() : x,y dimensions must not be 0");
    }
    
        
    inline iterator begin() {return data.begin();}
    inline const_iterator begin() const {return data.begin();}
    
    inline iterator end() {return data.end();}
    inline const_iterator end() const {return data.end();}
    
    
    inline size_t size() const
    {
      return data.size();
    }
    inline size_t X() const
    {
      return dimX;
    }
    inline size_t Y() const
    {
      return dimY;
    }
    
    inline T& operator()(size_t x, size_t y)
    {
      if (x>=dimX or y>=dimY)
        throw OutOfBounds("Matrix2D::operator() : Index out of bounds "+toString(x)+","+toString(y));
      return data[x + dimX*y];
    }
    inline const T& operator()(size_t x, size_t y) const
    {
      if (x>=dimX or y>=dimY)
        throw OutOfBounds("Matrix2D::operator() : Index out of bounds "+toString(x)+","+toString(y));
      return data[x + dimX*y];
    }
    
    inline T& operator[](size_t i)
    {
      if (i>=data.size())
        throw OutOfBounds("Matrix2D::operator[] : Index out of bounds "+toString(i));
      return data[i];
    }
    inline const T& operator[](size_t i) const
    {
      if (i>=data.size())
        throw OutOfBounds("Matrix2D::operator[] : Index out of bounds "+toString(i));
      return data[i];
    }
    
    
    inline void print(std::ostream& out = std::cout) const
    {
      out << toString(data);
    }
    
    inline T sum() const
    {
      T total = 0;
      for (const_iterator it=begin(); it<end(); ++it)
      {
        total += *it;
      }
      return total;
    }
    
};


/*
template<typename T>
inline T sum(const Matrix3D<T>& m)
{
  T total = 0;
  for (Matrix3D<T>::const_iterator it=m.begin(); it<m.end(); ++it)
  {
    total += *it;
  }
  return total;
}
*/

#endif
