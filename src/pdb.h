/*
     BANGalign : pseudo Bond and torsion ANGle aligner
     Copyright (C) 2009  Sebastian Kelm
 
     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PDB_H
#define PDB_H

#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <map>



class Residues
{
  std::map<std::string, unsigned char> code2letter;
  
  public:
    Residues()
    {
      code2letter["ALA"] = 'A';
      code2letter["CYS"] = 'C';
      code2letter["ASP"] = 'D';
      code2letter["GLU"] = 'E';
      code2letter["PHE"] = 'F';
      code2letter["GLY"] = 'G';
      code2letter["HIS"] = 'H';
      code2letter["ILE"] = 'I';
      code2letter["LYS"] = 'K';
      code2letter["LEU"] = 'L';
      code2letter["MET"] = 'M';
      code2letter["ASN"] = 'N';
      code2letter["PRO"] = 'P';
      code2letter["GLN"] = 'Q';
      code2letter["ARG"] = 'R';
      code2letter["SER"] = 'S';
      code2letter["THR"] = 'T';
      code2letter["VAL"] = 'V';
      code2letter["TRP"] = 'W';
      code2letter["TYR"] = 'Y';
      
      code2letter["MSE"] = 'M'; // Selenomethionine
      code2letter["SEC"] = 'U'; // Selenocysteine
      code2letter["ASX"] = 'B'; // ASP or ASN
      code2letter["XLE"] = 'J'; // LEU or ILE
      code2letter["GLX"] = 'Z'; // GLU or GLN
      code2letter["XAA"] = 'X'; // Unknown amino acid
      code2letter["UNK"] = 'X'; // Unknown amino acid
      code2letter["XXX"] = 'X'; // Unknown amino acid
      code2letter["U2X"] = 'Y'; // Modified Tyrosine with extra aromatic group linked to OH
      code2letter["1OP"] = 'Y';
      code2letter["U3X"] = 'F';
      code2letter["TY5"] = 'Y';
      code2letter["0A1"] = 'Y';
      code2letter["TY1"] = 'Y';
      code2letter["PTR"] = 'Y';
      code2letter["TYS"] = 'Y';
      code2letter["NLN"] = 'N';
      code2letter["OLS"] = 'S';
      code2letter["OLT"] = 'T';
      code2letter["CYX"] = 'C';
      code2letter["HIE"] = 'H';
      code2letter["MLY"] = 'K'; // N-dimethyl-lysine
    }
    
    inline unsigned char toLetter(const std::string& code)
    {
      std::map<std::string, unsigned char>::const_iterator it = code2letter.find( code );
      if ( it == code2letter.end() ) {
        return 'X';
      }
      else {
        return it->second;
      }
      //return code2letter[code];
    }
};

static Residues residues;


// Something with xyz coordinates
class Coord
{
  public:
    double x, y, z;

    Coord()
    : x(0), y(0), z(0)
    {
    }
    Coord(double X, double Y, double Z)
    : x(X), y(Y), z(Z)
    {
    }
    
    double& operator[](int i)
    {
      switch (i)
      {
        case 0: return x;
        case 1: return y;
        case 2: return z;
      }
      throw ValueError("Must not index a Coord with any integer other than 0,1,2.");
    }
    
    const double& operator[](int i) const
    {
      switch (i)
      {
        case 0: return x;
        case 1: return y;
        case 2: return z;
      }
      throw ValueError("Must not index a Coord with any integer other than 0,1,2.");
    }
    
    inline void assign(double X, double Y, double Z)
    {
      x = X;
      y = Y;
      z = Z;
    }
    
    inline void set_xyz(double* a)
    {
      x = a[0];
      y = a[1];
      z = a[2];
    }

    inline void get_xyz(double* a) const
    {
      a[0] = x;
      a[1] = y;
      a[2] = z;
    }

};


// Representation of a single ATOM in a PDB file
class Atom: public Coord
{
  public:
    double occup, b;
    int iatom, ires, resindex;
    std::string atom, res, chain, altloc, inscode, element;
    
    Atom() : Coord(0, 0, 0), occup(0), b(0), iatom(0), ires(0), resindex(-1) {};
    Atom(const std::string& line);
    
    void print(std::ostream& out = std::cout) const;
    
    int operator<(const Atom& other) const;
    int operator>(const Atom& other) const;
    Atom& operator=(const Atom& other);
    
    bool is_initialised() const
    {
      return (resindex >= 0);
    }
};

class Pdb
{
  public:
    Pdb() : residueCount(0) {};
    Pdb(const Pdb &pdb);
    Pdb(char* name);
    Pdb(const std::string name);
    Pdb(const std::string name, std::istream& in);
    Pdb(const char* name, const std::vector<std::string>* atype);
    Pdb(const std::string& name, const std::vector<std::string>* atype);
    Pdb(const std::string& name, std::istream& in, const std::vector<std::string>* atype);
    Pdb(const Pdb &pdb, const std::vector<std::string>* atype);
    

    inline const std::string getName() const {return name;}
    inline const std::vector<Atom>& getAtoms() const {return atoms;}
    
    inline unsigned int size() const {return atoms.size();}
    inline unsigned int countAtoms() const {return atoms.size();}
    unsigned int countResidues();
//     inline Atom& getAtom(int i) {return atoms[i];}
    
    Pdb copyByAtomType(std::string atype) const;
    void splitChains(std::vector<Pdb>& output, std::vector<std::string>& chainids) const;
    
    Atom& operator[](int x);
    const Atom& operator[](int x) const;
    
    inline void print(std::ostream& out = std::cout) const
    {
      for (unsigned i=0; i<atoms.size(); i++)
        atoms[i].print(out);
    }
    
    void sort();
    //inline void add(Atom &a) {atoms.push_back(a);}
    
    void add(Atom a);
    
    void initAtoms(std::istream& in);
    void initAtoms(std::istream& in, const std::vector<std::string>* atype);
    void initAtoms(const std::vector<Atom>& newatoms, const std::vector<std::string>* atype);

  private:
    std::string name;
    std::vector<Atom> atoms;
    unsigned int residueCount;
    
    Pdb(std::string name, std::vector<Atom>& a, int resCount=-1);
};


inline std::string toString(Atom a)
{
  std::stringstream ss;
  a.print(ss);
  return ss.str();
}
inline std::string toString(Pdb a)
{
  std::stringstream ss;
  a.print(ss);
  return ss.str();
}


inline std::string get_pdb_seq(const Pdb& query)
{
  std::string query_seq;
  for (unsigned i=0; i<query.size(); i++)
    query_seq += residues.toLetter(query[i].res);
  return query_seq;
}


double atom_distance(const Atom& a, const Atom& b);


#endif
