/*
     BANGalign : pseudo Bond and torsion ANGle aligner
     Copyright (C) 2009  Sebastian Kelm
 
     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BANG_H
#define BANG_H


#include <iostream>
#include <vector>
#include <cmath>

#include "skutil.h"     // my often-used utility functions
#include "matrices.h"   // 2D and 3D matrix implementations
#include "vectormath.h" // playing with 3d vectors and angles
#include "pdb.h"        // parsing PDB files
#include "rmsd.h"       // superpose vectors and calculate RMSD


const double PI = 3.14159265358979323846;
const double PI_OVER_2 = 1.57079632679489661923;



inline void write_to_file(const Pdb& p, std::string filename)
{
    std::ofstream out(filename.c_str());
    require(out, "ERROR: Could not write Pdb to file. Check permissions.");
    p.print(out);
    out.close();
}


const int blosum62[20][20] = {
// A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V
{  4,-1,-1,-2, 0,-1,-1, 0,-2,-1,-1,-1,-1,-2,-1, 1,-1,-3,-2,-2  },
{ -1, 5, 0,-2,-3, 1, 0,-2, 0,-3,-2 ,2,-1,-3,-2,-1,-1,-3,-2,-3  },
{ -2, 0, 6, 1,-3, 0, 0, 0,-1,-3,-3, 0,-2,-3,-2, 1, 0,-4,-2,-3  },
{ -2,-2, 1, 6,-3, 0, 2,-1,-1,-3,-4,-1,-3,-3,-1, 0, 1,-4,-3,-3  },
{  0,-3,-3,-3, 9,-3,-4,-3,-3,-1,-1,-3,-1,-2,-3,-1,-1,-2,-2,-1  },
{ -1, 1, 0, 0,-3, 5, 2,-2, 0,-3,-2, 1, 0,-3,-1, 0, 0,-2,-1,-2  },
{ -1, 0, 0, 2,-4, 2, 5,-2, 0,-3,-3, 1,-2,-3,-1, 0, 0,-3,-2,-3  },
{  0,-2,-2,-1,-3,-2,-2, 6,-2,-4,-4,-2,-3,-3,-2, 0, 1,-2,-3, 0  },
{ -2, 0, 1, 1,-3, 0, 0,-2, 8,-3,-3,-1,-2,-1,-2,-1, 0,-2, 2,-2  },
{ -1,-3,-3,-3,-1,-3,-3,-4,-3, 4, 2,-3, 1, 0,-3,-2,-2,-3,-1, 1  },
{ -1,-2,-3,-4,-1,-2,-3,-4,-3, 2, 4,-2, 2, 0,-3,-2,-2,-2,-1, 3  },
{ -1, 2, 0,-1,-3, 1, 1,-2,-1,-3,-2, 5,-1,-3,-1, 0, 0,-3,-2,-3  },
{ -1,-1,-2,-3,-1, 0,-2,-3,-2, 1, 2,-1, 5, 0,-2,-1,-1,-1,-1,-2  },
{ -2,-3,-3,-3,-2,-3,-3,-3,-1, 0, 0,-3, 0, 6,-4,-2,-2, 1, 3,-1  },
{ -1,-2,-1,-1,-3,-1,-1,-2,-2,-3,-3,-1,-2,-4, 7,-1, 1,-4,-3,-2  },
{  1,-1, 1, 0,-1, 0, 0, 0,-1,-2,-2, 0,-1,-2,-1, 4, 1,-3,-2,-2  },
{ -1,-1, 0, 1,-1, 0, 0, 1, 0,-2,-2, 0,-1,-2, 1, 1, 4,-3,-2,-2  },
{ -3,-3,-4,-4,-2,-2,-3,-2,-2,-3,-2,-3,-1, 1,-4,-3,-3,11, 2,-3  },
{ -2,-2,-2,-3,-2,-1,-2,-3, 2,-1,-1,-2,-1, 3,-3,-2,-2, 2, 7,-1  },
{  0,-3,-3,-3,-1,-2,-2,-3,-3, 3, 1,-2, 1,-1,-2,-2,-2,-3,-1, 4  }
};


int blosum_index(const char a)
{
    switch(a)
    {
      case 'A':
        return 0;
      case 'R':
        return 1;
      case 'N':
        return 2;
      case 'D':
        return 3;
      case 'C':
        return 4;
      case 'Q':
        return 5;
      case 'E':
        return 6;
      case 'G':
        return 7;
      case 'H':
        return 8;
      case 'I':
        return 9;
      case 'L':
        return 10;
      case 'K':
        return 11;
      case 'M':
        return 12;
      case 'F':
        return 13;
      case 'P':
        return 14;
      case 'S':
        return 15;
      case 'T':
        return 16;
      case 'W':
        return 17;
      case 'Y':
        return 18;
      case 'V':
        return 19;
      default:
        return -1;
    }
}

inline std::vector<int> get_blosum_indices(const Pdb& query)
{
    std::vector<int> seq;
    for (unsigned i=1; i<query.size()-1; ++i)
      seq.push_back(blosum_index(residues.toLetter(query[i].res)));
    return seq;
}



inline void swapchars(std::string& s, size_t i, size_t j)
{
  // XOR swap ... just being silly
  if (i!=j)
  {
    s[i] ^= s[j]; // i ^ j
    s[j] ^= s[i]; // j ^ (i ^ j) == i
    s[i] ^= s[j]; // (i ^ j) ^ i == j
  }
}


inline std::vector<Coord> make_dists(const Pdb& struc, int window=3)
{
  double d1;
  std::vector<Coord> dists(struc.size()-2);
  for (size_t i=1; i<struc.size()-2; i++)
  {
    for (size_t j=i+1; j<struc.size()-1; j++)
    {
      if((int)(j-i) < window and ((int)(struc[j].ires - struc[i].ires) < window)) continue;
      d1 = atom_distance(struc[i], struc[j]);
      if (d1 < 12.0) // two CA atoms within 8 Angstrom = a contact
      {
        if (d1 < 10.0) // two CA atoms within 8 Angstrom = a contact
        {
          if (d1 < 8.0) // two CA atoms within 8 Angstrom = a contact
          {
            dists[i-1].x += 1;
            dists[j-1].x += 1;
          }
          dists[i-1].y += 1;
          dists[j-1].y += 1;
        }
        dists[i-1].z += 1;
        dists[j-1].z += 1;
      }
    }
  }
  return dists;
}



class Aligner
{
    Pdb& query;
    Pdb& match;
    const std::vector<Coord> a_dists, b_dists;
    const std::vector<int> a_seq, b_seq;
    const std::vector<double> a_b;
    const std::vector<double> a_t;
    const std::vector<double> b_b;
    const std::vector<double> b_t;
    const int L, M, N; // window radius. Window size is 1 + 2L
    double gap_opening;
    double gap_elongation;
    const double angle_scaling;
    const double seq_weight, dist_weight;
    Matrix2D<double> scores;
    Matrix2D<unsigned char> arrows;
    double all_angle_simil;
    bool verbose;
    bool fasta;
    const double gap_opening_3d;
    const double gap_extension_3d;
    double D0, D0sq, D_super_gain, D_super_loss;

  public:
    Aligner(Pdb& query_pdb, Pdb& match_pdb, const std::vector<Coord>& query_dists, const std::vector<Coord>& match_dists, const std::vector<int>& a_sequence, const std::vector<int>& b_sequence, const std::vector<double>& a_bond, const std::vector<double>& a_torsion, const std::vector<double>& b_bond, const std::vector<double>& b_torsion, const int word_radius=6, const double gap_opening_score=-10, const double gap_elongation_score=-2.0, const double afactor=1.0, const double seq_weight_factor=0.0, const int seq_radius=0, const double dist_weight_factor=0.0, const int dist_radius=0, const double gap_opening_score_3d=-4, const double gap_extension_score_3d=-1, const double d_gain_factor=1.0, const double d_loss_factor=1.0)
    : query(query_pdb),
      match(match_pdb),
      a_dists(query_dists),
      b_dists(match_dists),
      a_seq(a_sequence),
      b_seq(b_sequence),
      a_b(a_bond), a_t(a_torsion), b_b(b_bond), b_t(b_torsion),
      L(abs(word_radius)),
      M(abs(seq_radius)),
      N(abs(dist_radius)),
      gap_opening(gap_opening_score),
      gap_elongation(gap_elongation_score),
      angle_scaling(PI*afactor),
      seq_weight(seq_weight_factor),
      dist_weight(dist_weight_factor),
      scores(a_bond.size()+1, b_bond.size()+1, 0),
      arrows(a_bond.size()+1, b_bond.size()+1, 'd'),
      all_angle_simil(-1),
      verbose(0), fasta(0),
      gap_opening_3d(gap_opening_score_3d),
      gap_extension_3d(gap_extension_score_3d)
    {
      require(a_b.size() == a_t.size(), "Aligner : angle vectors for query protein must have same length");
      require(b_b.size() == b_t.size(), "Aligner : angle vectors for match protein must have same length");
      require(a_b.size()+2 == query.size(), "Aligner : length(angle vectors) != length(query)-2");
      require(b_b.size()+2 == match.size(), "Aligner : length(angle vectors) != length(match)-2");
      require(word_radius >= 0, "Aligner : word radius must be > 0");
      require(seq_radius >= 0, "Aligner : seq radius must be >= 0");
      require(dist_radius >= 0, "Aligner : dist radius must be >= 0");
      require(angle_scaling > 0, std::string("Aligner : scaling factor must be > 0 ... factor provided: ") + toString(afactor));
      require(a_dists.size() == a_b.size(), "Aligner : distance vectors for match protein must have same length");
      require(b_dists.size() == b_b.size(), "Aligner : distance vectors for match protein must have same length");

      //std::cout << "Seq length vs angle length (query):" << a_seq.size() << " " << a_b.size() << std::endl;
      //std::cout << "Seq length vs angle length (match):" << b_seq.size() << " " << b_b.size() << std::endl;
      //std::cout << "BLOSUM indices (query):" << toString(a_seq) << std::endl;
      //std::cout << "BLOSUM indices (match):" << toString(b_seq) << std::endl;
      require(a_seq.size() == a_b.size(), "Aligner : sequence must have same length as angle vectors for query protein");
      require(b_seq.size() == b_b.size(), "Aligner : sequence must have same length as angle vectors for match protein");

      // Don't penalise starting gaps
      for (size_t x=1; x<scores.X(); ++x)
      {
          //scores(x,0) = scores(x-1,0) + gap_elongation;
          arrows(x,0) = 'l';
      }
      for (size_t y=1; y<scores.Y(); ++y)
      {
          //scores(0,y) = scores(0,y-1) + gap_elongation;
          arrows(0,y) = 'u';
      }
      D0 = 1.24 * pow((double)std::min(a_b.size(), b_b.size())-15, 1.0/3.0) - 1.8;
      D0sq = pow(D0, 2); // Normalisation factor from TMalign paper
      double D_superpose = std::min(std::max(4.5, D0), 8.0);
      D_super_gain = D_superpose * d_gain_factor;
      D_super_loss = D_superpose * d_loss_factor;
    }

  private:
    inline void word_diff(size_t x, size_t y, double& out_diff_bond, double& out_diff_torsion) //, double& out_diff_loopiness)
    {
      // Convert index to start with 0 rather than 1
      --x;
      --y;


      //unsigned size       = 1; // window size
      double total_weight = 0.0;
      double diff_torsion = 0.0; //pow(a_t[x+1] - b_t[y+1], 2);
      double diff_bond    = 0.0; //pow(a_b[x] - b_b[y], 2);

      // Extend window to the left
      for (int i=0; i>=-L and (unsigned)(x+i) < a_b.size() and (unsigned)(y+i) < b_b.size(); --i)
      {
        double weight = (1.0+L+abs(i))/(1.0+L);
        total_weight += weight;
        //++size;
        diff_bond    += pow(a_b[x+i]   - b_b[y+i],   2) * weight;
        diff_torsion += pow(a_t[x+i] - b_t[y+i], 2) * weight;
        //diff_torsion += pow(a_t[x+i+1] - b_t[y+i+1], 2) * weight;
          // We're adding 1 here, as we have one less torsion angle than
          // bond angles (the first torsion angle is set to 0).
      }
      // Extend window to the right
      for (int i=1; i<=L and (unsigned)(x+i+1) < a_b.size() and (unsigned)(y+i+1) < b_b.size(); ++i)
      {
        double weight = (1.0+L+abs(i))/(1.0+L);
        total_weight += weight;
        //++size;
        diff_bond    += pow(a_b[x+i] - b_b[y+i], 2);
        diff_torsion += pow(a_t[x+i] - b_t[y+i], 2);
      }

      //out_diff_bond = diff_bond/(double)size;
      //out_diff_torsion = (size > 1 ? diff_torsion/(double)(size-1) : 0);
      if (total_weight > 0)
      {
        out_diff_bond = diff_bond/total_weight;
        out_diff_torsion = diff_torsion/total_weight;
      }
    }

    inline double simil(double diff)
    {
      return (1.0 - std::min(angle_scaling, diff)/angle_scaling);
    }

    inline unsigned char direction(size_t x, size_t y)
    {
      return arrows(x,y);
      /*
      double up   = scores(x, y-1);
      double left = scores(x-1, y);
      double diag = scores(x-1, y-1);

      if (diag>=up)
      {
        if (diag>=left)
          return 'd';
        else
          return 'l';
      }
      else
      {
        if (up>=left)
          return 'u';
        else
          return 'l';
      }
      */
    }

    inline double blosum_score(size_t x, size_t y)
    {
      // Convert index to start with 0 rather than 1
      --x;
      --y;

      //unsigned size = 1; // actual window size
      double total_weight = 1.0;
      double score = (a_seq[x] >= 0 and b_seq[y] >=0) ? (blosum62[a_seq[x]][b_seq[y]]) : -1;
      // Extend window to the left
      for (int i=-1; i>=-M and (unsigned)(x+i) < a_seq.size() and (unsigned)(y+i) < b_seq.size(); --i)
      {
        //++size;
        double s = (a_seq[x+i] >= 0 and b_seq[y+i] >=0) ? (blosum62[a_seq[x+i]][b_seq[y+i]]) : -1;
        double weight = (1+M+abs(i))/(1+(double)M);
        total_weight += weight;
        score += s * weight;
      }
      // Extend window to the right
      for (int i=1; i<=M and (unsigned)(x+i) < a_seq.size() and (unsigned)(y+i) < b_seq.size(); ++i)
      {
        //++size;
        double s = (a_seq[x+i] >= 0 and b_seq[y+i] >=0) ? (blosum62[a_seq[x+i]][b_seq[y+i]]) : -1;
        double weight = (1+M+abs(i))/(1+(double)M);
        total_weight += weight;
        score += s * weight;
      }

      //return seq_weight * (score/(double)size);
      return seq_weight * (score/total_weight);
    }

    inline double dist_score(size_t x, size_t y)
    {
      // Convert index to start with 0 rather than 1
      --x;
      --y;
      
      double total_weight = 1.0;
      double score = distance(a_dists[x], b_dists[y]);
      // Extend window to the left
      for (int i=-1; i>=-N and (unsigned)(x+i) < a_seq.size() and (unsigned)(y+i) < b_seq.size(); --i)
      {
        //++size;
        double s = distance(a_dists[x+i], b_dists[y+i]);
        double weight = (1+N+abs(i))/(1+(double)N);
        total_weight += weight;
        score += s * weight;
      }
      // Extend window to the right
      for (int i=1; i<=N and (unsigned)(x+i) < a_seq.size() and (unsigned)(y+i) < b_seq.size(); ++i)
      {
        //++size;
        double s = distance(a_dists[x+i], b_dists[y+i]);
        double weight = (1+N+abs(i))/(1+(double)N);
        total_weight += weight;
        score += s * weight;
      }

      //return seq_weight * (score/(double)size);
      return -dist_weight * (score/total_weight);
    }

    inline double gapscore(size_t x, size_t y, unsigned char newdir)
    {
      if (direction(x, y) == newdir)
        return gap_elongation;
      else
        return gap_opening + gap_elongation;
    }

    inline double prevscore(size_t x, size_t y, double gapfactor=1.0)
    {
      double up   = scores(x, y-1);
      double left = scores(x-1, y);
      double diag = scores(x-1, y-1);

      if (diag>=up)
      {
        if (diag>=left)
        {
          arrows(x,y) = 'd';
          return diag; // Note: arrow matrix is already initialised with diagonals
        }
        else
        {
          arrows(x,y) = 'l';
          return left + gapfactor * gapscore(x-1, y, 'l');
        }
      }
      else
      {
        if (up>=left)
        {
          arrows(x,y) = 'u';
          return up + gapfactor * gapscore(x, y-1, 'u');
        }
        else
        {
          arrows(x,y) = 'l';
          return left + gapfactor * gapscore(x-1, y, 'l');
        }
      }
    }

    inline void fill_step(size_t x, size_t y, double gapfactor, double& totalbond, double& totaltorsion)
    {
      double bondmatch=0, torsionmatch=0;
      word_diff(x, y, bondmatch, torsionmatch);
      totalbond    += bondmatch;
      totaltorsion += torsionmatch;

      // Score is normalised to interval [-10, 10]
      //if (x == y)
      //{
      //  std::cout << "DEBUG:" << x << " " << simil(sqrt(bondmatch)) << " " <<  simil(sqrt(torsionmatch))  << " " << prevscore(x, y, gapfactor) << std::endl;
      //}
      double angle_score;
      if (x>=scores.X()-1 or y>=scores.Y()-1)
        angle_score = 0.0;
      else
        angle_score = simil(sqrt(bondmatch))*simil(sqrt(torsionmatch)) * 20.0 - 10.0;
      
      scores(x,y) = blosum_score(x, y) + dist_score(x, y) + angle_score + prevscore(x, y, gapfactor);
      //if (x>=scores.X()-2 and y>=scores.Y()-2)
      //  std::cout << x  << " " << y << " :" << scores(x,y) << " = " << blosum_score(x, y) << " + " << dist_score(x, y) << " + " << simil(sqrt(bondmatch)) << "(" << bondmatch << ") * " << simil(sqrt(torsionmatch)) << "(" << torsionmatch << ") * 20 - 10 + " << prevscore(x, y, gapfactor) << "\n";
    }


  public:
    
    void fill_matrix()
    {
      double totalbond=0, totaltorsion=0;

      //std::cout << "Before: ";
      //for (size_t y=1; y<arrows.Y(); ++y)
      //{
      //  std::cout << arrows(y,y);
      //}
      //std::cout << "\n";

      for (size_t x=1; x<scores.X()-1; ++x)
        for (size_t y=1; y<scores.Y()-1; ++y)
          fill_step(x, y, 1.0, totalbond, totaltorsion);

      // endgaps not penalised:
      for (size_t x=1; x<scores.X()-1; ++x)
          fill_step(x, scores.Y()-1, 0.0, totalbond, totaltorsion);

      // endgaps not penalised:
      for (size_t y=1; y<scores.Y(); ++y)
          fill_step(scores.X()-1, y, 0.0, totalbond, totaltorsion);

      all_angle_simil =
        simil(sqrt(totalbond/(scores.X()-1)/(scores.Y()-1))) +
        simil(sqrt(totaltorsion/(scores.X()-1)/(scores.Y()-1)));
      all_angle_simil /= 2;
      
      //std::cout << "After: ";
      //for (size_t y=1; y<arrows.Y(); ++y)
      //{
      //  std::cout << arrows(y,y);
      //}
      //std::cout << "\n";
    }
    
    void get_gapless_alignment(size_t i, size_t j, std::vector<size_t>& a_ali, std::vector<size_t>& b_ali)
    {
      //std::cout << "get_gapless_alignment(" << i << ", " << j << ")\n";
      a_ali.clear();
      b_ali.clear();
      size_t x=scores.X()-1;
      size_t y=scores.Y()-1;
      size_t x_start, y_start, x_end, y_end;
      if (x>y)
      {
        x_start = j+y;
        x_end = j;
        y_start = y;
        y_end = 0;
      }
      else
      {
        x_start = x;
        x_end = 0;
        y_start = i+x;
        y_end = i;
      }
      require(x_start-x_end == y_start-y_end, "BUG: gaplessly aligned sequence stretches are not of the same length");
      for (x=x_start, y=y_start; (x != 0) and (y != 0); --x, --y)
      {
        //std::cout << "get_gapless_alignment: x=" << x << ", y=" << y << "\n";
        a_ali.push_back(x-1);
        b_ali.push_back(y-1);
      }
      //std::cout << "get_gapless_alignment(" << i << ", " << j << ") ends\n";
    }
    
    void fill_combined_matrix_3d()
    {
        // TODO ...
        // fill score matrix with average of two given matrices
        // deal with the prevscore() at the same time
    }

    
    
    inline void set_output_options(bool verbose, bool fasta)
    {
      this->verbose = verbose;
      this->fasta = fasta;
    }

    inline double get_all_angle_simil()
    {
      return all_angle_simil;
    }


    unsigned get_alignment(std::vector<size_t>& a_ali, std::vector<size_t>& b_ali)
    {
      a_ali.clear();
      b_ali.clear();
      size_t x=scores.X()-1;
      size_t y=scores.Y()-1;
      int N=0;

      //std::cout << "Alignment: ";
      while (x>0 or y>0)
      {
        unsigned char arrow = direction(x,y);
        switch (arrow)
        {
          case 'd': // diagonal
            a_ali.push_back(x-1);
            b_ali.push_back(y-1);
            --x;
            --y;
            ++N;
            //std::cout << "d";
            break;
          case 'l': // left
            a_ali.push_back(x-1);
            b_ali.push_back(-1);
            --x;
            //std::cout << "l";
            break;
          case 'u': // up
            a_ali.push_back(-1);
            b_ali.push_back(y-1);
            --y;
            //std::cout << "u";
            break;
          default:
            throw IllegalState("BUG: during backtrace, got an illegal arrow...\n");
        }
      }
      
      return N;
    }
    
    unsigned get_alignment(std::vector<size_t>& a_ali, std::vector<size_t>& b_ali, std::vector<double>& equiv)
    {
      a_ali.clear();
      b_ali.clear();
      size_t x=scores.X()-1;
      size_t y=scores.Y()-1;
      int N = 0;

      //std::cout << "Alignment: ";
      while (x>0 or y>0)
      {
        unsigned char arrow = direction(x,y);
        switch (arrow)
        {
          case 'd': // diagonal
            a_ali.push_back(x-1);
            b_ali.push_back(y-1);
            equiv.push_back(scores(x,y)-scores(x-1, y-1));
            --x;
            --y;
            ++N;
            //std::cout << "d";
            break;
          case 'l': // left
            a_ali.push_back(x-1);
            b_ali.push_back(-1);
            equiv.push_back(scores(x,y)-scores(x-1, y));
            --x;
            //std::cout << "l";
            break;
          case 'u': // up
            a_ali.push_back(-1);
            b_ali.push_back(y-1);
            equiv.push_back(scores(x,y)-scores(x, y-1));
            --y;
            //std::cout << "u";
            break;
          default:
            throw IllegalState("BUG: backtrace() got an illegal arrow...\n");
        }
      }
      //std::cout << "\n";

      /*
      while (x>0) // left (starting gap)
      {
        a_ali.push_back(x-1);
        b_ali.push_back(-1);
        equiv.push_back(scores(x,y)-scores(x-1, y));
        --x;
      }

      while (y>0) // up (starting gap)
      {
        a_ali.push_back(-1);
        b_ali.push_back(y-1);
        equiv.push_back(scores(x,y)-scores(x, y-1));
        --y;
      }
      */

      return N;
    }
    
    
    inline double get_score()
    {
      return scores(scores.X()-1, scores.Y()-1);
    }
    
    
    void print_aligned_sequences(const std::vector<size_t>& query_ali, const std::vector<size_t>& match_ali)
    {
      //Aligner ali(make_dists(query, dist_range), make_dists(match, dist_range), query_seq, match_seq, query_bond, query_dihedral, match_bond, match_dihedral, word_radius, gap_opening_score, gap_elongation_score, scaling_factor, seq_factor, seq_radius, dist_factor, dist_radius);
      //ali.fill_matrix();
    
      // Retrieve best alignment
      //
      // NOTE: This alignment is backwards!!
      //       And there's one value for every bond, not every residue!!!
      //
      std::string query_aligned, match_aligned;
      std::vector<double> equivalence;
      get_aligned_sequences(query_ali, match_ali, -1.0, query_aligned, match_aligned, equivalence, true);
    }
    void get_aligned_sequences(std::string& query_aligned, std::string& match_aligned, std::vector<double>& equivalence, bool doprint)
    {
      //Aligner ali(make_dists(query, dist_range), make_dists(match, dist_range), query_seq, match_seq, query_bond, query_dihedral, match_bond, match_dihedral, word_radius, gap_opening_score, gap_elongation_score, scaling_factor, seq_factor, seq_radius, dist_factor, dist_radius);
      //ali.fill_matrix();
    
      // Retrieve best alignment
      //
      // NOTE: This alignment is backwards!!
      //       And there's one value for every bond, not every residue!!!
      //
      std::vector<size_t> query_ali;
      std::vector<size_t> match_ali;
      get_alignment(query_ali, match_ali, equivalence);
      get_aligned_sequences(query_ali, match_ali, get_score(), query_aligned, match_aligned, equivalence, doprint);
    }
    
    void get_aligned_sequences(const std::vector<size_t>& query_ali, const std::vector<size_t>& match_ali, const double score, std::string& query_aligned, std::string& match_aligned, std::vector<double>& equivalence, bool doprint)
    {
      // Sanity check
      //
      require(query_ali.size() == match_ali.size(), "print_alignment() : sequences not aligned!");
    
      //std::cout << toString(query_ali) << endl;
      //std::cout << toString(match_ali) << endl;
    
    
      // Rebuild the aligned amino acid sequence
      //
      // The angles we have aligned correspond to all CA atoms apart from the first and last residues.
      //
    
      // Insert first residue (which isn't in the alignment)
      //
      query_aligned += residues.toLetter(query[0].res);
      match_aligned += residues.toLetter(match[0].res);
    
      // Insert aligned residues
      //
      const unsigned offset = 1;
      for (size_t i=0; i<query_ali.size(); i++)
      {
        size_t n = query_ali.size() - 1 - i;
    
        char res1 = '-';
        char res2 = '-';
    
        if (query_ali[n] != (size_t)-1)
          res1 = residues.toLetter(query[query_ali[n] + offset].res);
          //std::cout << query[query_ali[n] + offset].res << " " << res1 << endl;
        if (match_ali[n] != (size_t)-1)
          res2 = residues.toLetter(match[match_ali[n] + offset].res);
          //std::cout << match[match_ali[n] + offset].res << " " << res2 << endl;
    
        query_aligned += res1;
        match_aligned += res2;
        //std::cout << "TestQ='" << query_aligned << "'\n";
        //std::cout << "TestM='" << match_aligned << "'\n";
      }
    
      // Insert last residue (which isn't in the alignment)
      //
      query_aligned += residues.toLetter(query[query.size()-1].res);
      match_aligned += residues.toLetter(match[match.size()-1].res);
      //std::cout << "TestQ='" << query_aligned << "'\n";
      //std::cout << "TestM='" << match_aligned << "'\n";
    
    
      // Move first and last residues inwards, until they hit the first non-gap
      //
      if (query_aligned.size() > 2)
      {
        size_t n;
    
        n = query_aligned.find_first_not_of('-', 1);
        swapchars(query_aligned, 0, n-1);
    
        n = match_aligned.find_first_not_of('-', 1);
        swapchars(match_aligned, 0, n-1);
    
        n = query_aligned.find_last_not_of('-', query_aligned.size()-2);
        swapchars(query_aligned, query_aligned.size()-1, n+1);
    
        n = match_aligned.find_last_not_of('-', match_aligned.size()-2);
        swapchars(match_aligned, match_aligned.size()-1, n+1);
      }
      
      size_t id=0, gaps=0;
      for (size_t i=0; i<query_aligned.size(); i++)
      {
        char res1 = query_aligned[i];
        char res2 = match_aligned[i];
        if ('-' == res1 or '-' == res2)
          gaps++;
        else if (res1 == res2)
          id++;
      }
    
    
      /*
          TODO: We may have just created a misalignment, by moving the first/last residue inwards. We need to insert a gap, that fixes this...
      */
      
      if (doprint)
      {
        if (fasta)
        {
            std::cout << ">" << query.getName() << "\n" << query_aligned << "\n";
            std::cout << ">" << match.getName() << "\n" << match_aligned << "\n";
        }
        else
        {
            std::cout << "Query file=" << query.getName() << "\n";
            std::cout << "Match file=" << match.getName() << "\n";
            std::cout << "Query=" << query_aligned << "\n";
            std::cout << "Match=" << match_aligned << "\n";
            if (verbose)
            {
                std::cout << "Query length=" << query.size() << "\n";
                std::cout << "Match length=" << match.size() << "\n";
                if (equivalence.size())
                {
                  std::cout << "Equivalence=0.0,";
                  for (size_t i=1; i<=equivalence.size(); ++i)
                    std::cout << toString(equivalence[equivalence.size()-i]) << ",";
                  std::cout << "0.0\n";
                  std::cout << "All-angle similarity (alignment independent)=" << get_all_angle_simil() << "\n";
                  std::cout << "Alignment score (absolute)="   << score << "\n";
                  std::cout << "Alignment score (over alignment length)=" << score/query_aligned.size() << "\n";
                }
                std::cout << "Coverage (absolute)="   << query_aligned.size() - gaps << "\n";
                std::cout << "Coverage (over alignment length)=" << double(query_aligned.size()-gaps)/query_aligned.size() << "\n";
                std::cout << "ID (absolute)="   << id << "\n";
                std::cout << "ID (over alignment length)=" << double(id)/query_aligned.size() << "\n";
      
              //   std::cout << "%ID (over shorter length)=" << double(id)/min(query.size(), match.size()) << "\n";
              //   std::cout << "%ID (over longer length)=" << double(id)/max(query.size(), match.size()) << "\n";
              //   std::cout << "%ID (over average length)=" << double(id)/(double(query.size() + match.size())/2) << "\n";
              //   std::cout << "%ID (over query length)=" << double(id)/query.size() << "\n";
              //   std::cout << "%ID (over match length)=" << double(id)/match.size() << "\n";
            }
        }
      }
    }
    
  private:
    void transform(Pdb& struc, double U[3][3], double mov_com[3], double mov_to_ref[3])
    {
      double originl[3] = {0.0, 0.0, 0.0};
      double rotated[3] = {0.0, 0.0, 0.0};
      for (size_t n=0; n<struc.size(); ++n) 
      {
        for (size_t i=0; i<3; ++i)
          originl[i] = struc[n][i] - mov_com[i];
        for (size_t i=0; i<3; ++i)
        {
          rotated[i] = 0.0; 
          for (size_t j=0; j<3; ++j)
            rotated[i] += U[i][j] * originl[j];
        }
        struc[n].x = rotated[0] + mov_com[0] + mov_to_ref[0];
        struc[n].y = rotated[1] + mov_com[1] + mov_to_ref[1];
        struc[n].z = rotated[2] + mov_com[2] + mov_to_ref[2];
      }
    }
    
    inline void fill_step_3d(size_t x, size_t y, double gapfactor)
    {
      //const double scale = D0sq;
      double Dsq = square_distance(query[x], match[y]);
      double tm = 1.0/(1.0 + Dsq/D0sq);
      //double D = distance(query[x], match[y]);
      //double tm = 1.0/(1.0 + pow(D/D0, 2));
      //double tm = std::max(0.0, scale-Dsq)/scale * 20.0 - 10.0;
      scores(x,y) = tm + prevscore(x, y, gapfactor);
    }
    
    void fill_matrix_3d()
    {
      double old_open = gap_opening;
      double old_elong = gap_elongation;
      gap_opening = gap_opening_3d;
      gap_elongation = gap_extension_3d;
      
      for (size_t x=1; x<scores.X()-1; ++x)
        for (size_t y=1; y<scores.Y()-1; ++y)
          fill_step_3d(x, y, 1.0);

      // endgaps not penalised:
      for (size_t x=1; x<scores.X()-1; ++x)
          fill_step_3d(x, scores.Y()-1, 0.0);

      // endgaps not penalised:
      for (size_t y=1; y<scores.Y(); ++y)
          fill_step_3d(scores.X()-1, y, 0.0);
      
      gap_opening = old_open;
      gap_elongation = old_elong;
    }
    
    void get_coords(const std::vector<size_t>& query_ali, const std::vector<size_t>& match_ali, std::vector<Coord>& query_coords, std::vector<Coord>& match_coords)
    {
      require(query_ali.size() == match_ali.size(), "get_coords : aligned sequence vectors must be of same length");
      const unsigned offset = 1;
      query_coords.clear();
      match_coords.clear();
      for (size_t i=0; i<query_ali.size(); ++i)
      {
        if ((query_ali[i] != (size_t)-1) and (match_ali[i] != (size_t)-1))
        {
          //not a gap
          query_coords.push_back(query[query_ali[i]+offset]);
          match_coords.push_back(match[match_ali[i]+offset]);
        }
      }
    }
    
    double get_TM_score(const std::vector<Coord>& query_coords, const std::vector<Coord>& match_coords)
    {
      require(query_coords.size() == match_coords.size(), "Can't calculate TM score on two non-equal length lists of coordinates");
      double tm = 0.0;
      int L = std::min(a_b.size(), b_b.size());
      for (size_t i=0; i<query_coords.size(); ++i)
      {
        tm += 1.0 / (1.0 + pow((distance(query_coords[i], match_coords[i]) / D0), 2));
        //tm += 1.0 / (1.0 + square_distance(query_coords[i], match_coords[i]) / D0sq);
      }
      return tm / L;
    }
    
    double get_TM_score(const std::vector<size_t>& query_ali, const std::vector<size_t>& match_ali)
    {
      require(query_ali.size() == match_ali.size(), "Can't calculate TM score on two non-equal length lists of coordinates");
      const unsigned offset = 1;
      double tm = 0.0;
      int L = std::min(a_b.size(), b_b.size());
      for (size_t i=0; i<query_ali.size(); ++i)
      {
        if ((query_ali[i] != (size_t)-1) and (match_ali[i] != (size_t)-1))
        {
          //not a gap
          tm += 1.0 / (1.0 + pow((distance(query[query_ali[i]+offset], match[match_ali[i]+offset]) / D0), 2));
          //tm += 1.0 / (1.0 + square_distance(query[query_ali[i]+offset], match[match_ali[i]+offset]) / D0sq);
        }
      }
      return tm / L;
    }
    
    
    void get_aligned_indices(const std::vector<size_t>& query_ali, const std::vector<size_t>& match_ali, std::vector<size_t>& query_indices, std::vector<size_t>& match_indices)
    {
      require(query_ali.size() == match_ali.size(), "get_coords : aligned sequence vectors must be of same length");
      const unsigned offset = 1;
      query_indices.clear();
      match_indices.clear();
      for (size_t i=0; i<query_ali.size(); ++i)
      {
        if ((query_ali[i] != (size_t)-1) and (match_ali[i] != (size_t)-1))
        {
          //not a gap
          query_indices.push_back(query_ali[i]+offset);
          match_indices.push_back(match_ali[i]+offset);
        }
      }

    }


    void get_biggest_consecutive_chunk(std::vector<size_t>& query_indices, std::vector<size_t>& match_indices)
    {
      require(query_indices.size() == match_indices.size(), "get_biggest_consecutive_chunk : aligned sequence vectors must be of same length");
      size_t start=0, end=0, i=0;
      for (i=1; i<query_indices.size(); ++i)
      {
        if ((query_indices[i-1]-query_indices[i] != (size_t)1) or (match_indices[i-1]-match_indices[i] != (size_t)1))
        {
          // gap
          if (start-end < i-start)
          {
            start=end;
            end=i;
          }
        }
      }
      if (start-end < i-start)
      {
        start=end;
        end=i;
      }
      //std::cout << "before: " << toString(query_indices) << "\n";
      query_indices.erase(query_indices.begin()+end, query_indices.end());
      query_indices.erase(query_indices.begin(),     query_indices.begin()+start);
      match_indices.erase(match_indices.begin()+end, match_indices.end());
      match_indices.erase(match_indices.begin(),     match_indices.begin()+start);
      //std::cout << "after:  " << toString(query_indices) << "\n";
    }
    
    
    bool reseed_close_residue_pairs(const std::vector<size_t> query_indices, const std::vector<size_t> match_indices, std::vector<size_t>& query_seed_indices, std::vector<size_t>& match_seed_indices, std::vector<bool>& seed_map)
    {
      bool changed = false;
      query_seed_indices.clear();
      match_seed_indices.clear();
      for (size_t i=0; i<query_indices.size(); ++i)
      {
        double d = distance(query[query_indices[i]], match[match_indices[i]]);
        //if (d < D_superpose)
        if (seed_map[i])
        {
          if (d < D_super_loss)
          {
            query_seed_indices.push_back(query_indices[i]);
            match_seed_indices.push_back(match_indices[i]);
          }
          else
          {
            seed_map[i] = false;
            changed = true;
            //std::cout << "Checkpoint 07a : removing residue " << i << " : q " << query_indices[i] << " m " << match_indices[i] << "\n";
          }
        }
        else if (d < D_super_gain)
        {
          query_seed_indices.push_back(query_indices[i]);
          match_seed_indices.push_back(match_indices[i]);
          seed_map[i] = true;
          changed = true;
          //std::cout << "Checkpoint 07a : adding residue " << i << " : q " << query_indices[i] << " m " << match_indices[i] << "\n";
        }
      }
      return changed;
    }
    
    void superpose(const std::vector<size_t>& query_ali, const std::vector<size_t>& match_ali, double& out_tm, double& out_rmsd)
    {
      std::vector<size_t> query_indices;
      std::vector<size_t> match_indices;
      get_aligned_indices(query_ali, match_ali, query_indices, match_indices);
      size_t Naligned = query_indices.size();
      //std::cout << "Checkpoint 01 : Naligned = " << Naligned << "\n";
      
      double best_tm = -1.0;
      std::vector<size_t> best_query_seed_indices;
      std::vector<size_t> best_match_seed_indices;
      
      std::vector<size_t> query_seed_indices;
      std::vector<size_t> match_seed_indices;
      std::vector<bool> seed_map(query_indices.size());
      std::vector<Coord> query_seed;
      std::vector<Coord> match_seed;
      for (size_t L=Naligned; L>=4+Naligned/3; L/=2)
      {
        //std::cout << "Checkpoint 02 : Naligned = " << Naligned << " L = " << L << "\n";
        for (size_t i=(Naligned%L)/2; i<=Naligned-L; i+=L)
        {
          //std::cout << "Checkpoint 03: Naligned = " << Naligned << " L = " << L << " i = " << i << "\n";
          // Set the seed indices and maps
          query_seed_indices.clear();
          match_seed_indices.clear();
          for (size_t j=0; j<i; ++j)
            seed_map[i] = false;
          for (size_t j=i; j<i+L; ++j)
          {
            query_seed_indices.push_back(query_indices[j]);
            match_seed_indices.push_back(match_indices[j]);
            seed_map[j] = true;
          }
          for (size_t j=i+L; j<Naligned; ++j)
            seed_map[i] = false;
          
          //get_biggest_consecutive_chunk(query_seed_indices, match_seed_indices);
          if (query_seed_indices.size()<4)
            continue;
          
          // Do iterative re-alignment and keep the alignment with the best TM-score
          bool alignment_changed = true;
          for(unsigned inner_iterations = 0; alignment_changed and (inner_iterations < 10); ++inner_iterations)
          {
            double tm, rmsd;
            quick_superpose(query_ali, match_ali, query_seed_indices, match_seed_indices, tm, rmsd);
            if (tm <= 0)
              break;
            
            if (tm > best_tm)
            {
              //std::cout << "Checkpoint 06 : best_tm := " << tm << "\n";
              best_tm = tm;
              best_query_seed_indices = query_seed_indices;
              best_match_seed_indices = match_seed_indices;
            }
            alignment_changed = reseed_close_residue_pairs(query_indices, match_indices, query_seed_indices, match_seed_indices, seed_map);
            //std::cout << "Checkpoint 07 : alignment_changed = " << alignment_changed << "\n";
          }
        }
      }
      if (best_tm > 0)
      {
        //std::cout << "Checkpoint 08\n";
        // Restore the best alignment
        quick_superpose(query_ali, match_ali, best_query_seed_indices, best_match_seed_indices, out_tm, out_rmsd);
      }
      else
      {
        //std::cout << "Checkpoint 09\n";
        out_tm = -1;
        out_rmsd = 999999;
      }
    }
    
    
    // DEBUG: No idea why this doesn't work the same in a function or outside
    void quick_superpose(const std::vector<size_t>& query_ali, const std::vector<size_t>& match_ali, const std::vector<size_t>& query_seed_indices, const std::vector<size_t>& match_seed_indices, double& out_tm, double& out_rmsd)
    {
      double mov_com[3] = {0.0, 0.0, 0.0};
      double mov_to_ref[3] = {0.0, 0.0, 0.0};
      double U[3][3] = {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}};
      std::vector<Coord> query_seed, match_seed;
      get_coords(query_seed_indices, match_seed_indices, query_seed, match_seed);
      if (query_seed.size() < 4)
      {
        out_rmsd = 999999;
        out_tm = -1;
        return;
      }
      out_rmsd = calculate_rotation_rmsd(query_seed, match_seed, mov_com, mov_to_ref, U);
      transform(match, U, mov_com, mov_to_ref);
      out_tm = get_TM_score(query_ali, match_ali);
    }
    
    
    double move_allatom(Pdb& desired_position, Pdb& match_allatom)
    {
      double mov_com[3] = {0.0, 0.0, 0.0};
      double mov_to_ref[3] = {0.0, 0.0, 0.0};
      double U[3][3] = {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}};
      const std::vector<std::string> atomTypes(1, "CA");
      Pdb match_allatom_CA(match_allatom, &atomTypes);
      require(match_allatom_CA.size() == desired_position.size());
      
      std::vector<Coord> ref_coords;
      std::vector<Coord> mov_coords;
      for (size_t i=0; i<desired_position.size(); ++i)
      {
        ref_coords.push_back(desired_position[i]);
        mov_coords.push_back(match_allatom_CA[i]);
      }
      
      double rmsd = calculate_rotation_rmsd(ref_coords, mov_coords, mov_com, mov_to_ref, U);
      transform(match_allatom, U, mov_com, mov_to_ref);
      return rmsd;
    }
    
  
  
  public:
    double superpose(Pdb& match_allatom, const std::string& moved_filename, bool refine)
    {
      require(query.size()-2 >= 4 and match.size()-2 >= 4, "Input proteins too short to align!");
      const double default_gap_opening = gap_opening;
      const double default_gap_elongation = gap_elongation;
      
      //////////////////////////////////////////
      // Do angle & contact based alignment
      //////////////////////////////////////////
      std::vector<size_t> best_query_ali;
      std::vector<size_t> best_match_ali;
      double best_tm_score = -1.0;
      double best_rmsd = 9999999.0;
      Pdb best_position = match;
      
      std::vector<size_t> query_ali;
      std::vector<size_t> match_ali;
      double tm = -1.0;
      double rmsd = 9999999.0;
      unsigned N;
      
      
      // Gapless seed alignment
      //
      gap_opening = -99999999;
      gap_elongation = -99999;
      fill_matrix(); // angle based initial alignment
      N = get_alignment(query_ali, match_ali); // this traces the DP matrix for the optimal alignment
      if (N >= 4)
      {
        superpose(query_ali, match_ali, tm, rmsd);
        if (verbose and !fasta)
        {
          std::cout << "BANG gapless seed alignment\n";
          std::cout << "BANG superpose : N-aligned: " << N << ": RMSD " << rmsd << "; TM-score " << tm << "\n";
        }
        
        if (tm > best_tm_score)
        {
          best_query_ali = query_ali;
          best_match_ali = match_ali;
          best_tm_score = tm;
          best_rmsd = rmsd;
          best_position = match;
        }
      }      

      
      // Try various gap penalties for seed alignments
      //
      for (gap_opening=default_gap_opening; gap_opening > -4; gap_opening+=gap_opening)
      {
        double prev_tm = best_tm_score;
        for (gap_elongation=default_gap_elongation; gap_elongation > gap_opening; gap_elongation += gap_opening/2)
        {
          
          // Try different seed alignments
          //
          if (verbose and !fasta)
            std::cout << "BANG seed alignment : gap penalties: " << gap_opening << ", " << gap_elongation << "\n";
          fill_matrix(); // angle based initial alignment

          N = get_alignment(query_ali, match_ali); // this traces the DP matrix for the optimal alignment
          if (N < 4)
            continue;
          superpose(query_ali, match_ali, tm, rmsd);
          
          // One round of refinement, if TM is decent
          /*
          if (tm > 0.5)
          {
            fill_matrix_3d();  // this generates a new DP matrix from the current superposition
            N = get_alignment(query_ali, match_ali); // this traces the DP matrix for the optimal alignment
            if (N < 4)
              continue;
            superpose(query_ali, match_ali, tm, rmsd);
          }
          */
          
          if (verbose and !fasta)
          {
            std::cout << "BANG superpose : N-aligned: " << N << ": RMSD " << rmsd << "; TM-score " << tm << "\n";
          }
          
          if (tm > best_tm_score)
          {
            best_query_ali = query_ali;
            best_match_ali = match_ali;
            best_tm_score = tm;
            best_rmsd = rmsd;
            best_position = match;
          }
        }
        if (tm < best_tm_score and tm < prev_tm)
          break;
      }
      
      
      // Restore best position from the above
      match = best_position;
      
      
      // Refine alignment using 3D coordinates
      //
      tm = best_tm_score;
      double prev_tm=-1.0;
      for (unsigned iteration=0; refine and (iteration<10) and (tm > prev_tm) and (tm > 0.17) and (best_tm_score < 0.9999); ++iteration)
      {
        prev_tm = tm;
        fill_matrix_3d();  // this generates a new DP matrix from the current superposition
        N = get_alignment(query_ali, match_ali); // this traces the DP matrix for the optimal alignment
        if (N < 4)
          break;
        superpose(query_ali, match_ali, tm, rmsd);
        
        if (verbose and !fasta)
        {
          std::cout << "BANG refinement " << iteration << ": N-aligned: " << N << ": RMSD " << rmsd << "; TM-score " << tm << "\n";
        }
        if (tm > best_tm_score)
        {
          best_query_ali = query_ali;
          best_match_ali = match_ali;
          best_tm_score = tm;
          best_rmsd = rmsd;
          best_position = match;
        }
      }

      
      /*
      for (unsigned big_iteration = 0; (big_iteration < 3) and (best_tm_score < 0.99); ++big_iteration)
      {
        // Try different seed alignments
        //
        double old_gap_opening = gap_opening;
        double old_gap_elongation = gap_elongation;
        switch(big_iteration)
        {
          case 0:
          {
            if (verbose and !fasta)
              std::cout << "BANG alignment, round " << (big_iteration+1) << " : gap penalties: " << gap_opening << ", " << gap_elongation << "\n";
            fill_matrix(); // angle based initial alignment
            break;
          }
          case 1:
          {
            gap_opening -= 7;
            gap_elongation -= 2;
            if (verbose and !fasta)
              std::cout << "BANG alignment, round " << (big_iteration+1) << " : gap penalties: " << gap_opening << ", " << gap_elongation << "\n";
            fill_matrix(); // angle based initial alignment
            break;
          }
          case 2:
          {
            gap_opening = -99999999;
            gap_elongation = -99999;
            if (verbose and !fasta)
              std::cout << "BANG alignment, round " << (big_iteration+1) << " : gap penalties: " << gap_opening << ", " << gap_elongation << "\n";
            fill_matrix();
            break;
          }
        }
        gap_opening = old_gap_opening;
        gap_elongation = old_gap_elongation;
        // Try to optimise the alignment using the 3D coordinates
        //
        std::vector<size_t> query_ali;
        std::vector<size_t> match_ali;
        double prev_tm = -1.0;
        double tm = -1.0;
        double rmsd = 9999999.0;
        unsigned N = 0;
        unsigned iteration=0;
        do
        {
          N = get_alignment(query_ali, match_ali); // this traces the DP matrix for the optimal alignment
          
          if (N < 4)
            break;
          prev_tm = tm;
          superpose(query_ali, match_ali, tm, rmsd);
          fill_matrix_3d();  // this generates a new DP matrix from the current superposition
          
          if (verbose and !fasta)
          {
            std::cout << "BANG iteration " << iteration << ": N-aligned: " << N << ": RMSD " << rmsd << "; TM-score " << tm << "\n";
          }
          if (tm > best_tm_score)
          {
            best_query_ali = query_ali;
            best_match_ali = match_ali;
            best_tm_score = tm;
            best_rmsd = rmsd;
            best_position = match;
            //scores1=scores; // copy score matrix
            //arrows1=arrows; // copy arrow matrix
          }
          ++iteration;
        }
        while (refine and (iteration<10) and (tm > prev_tm) and (tm > 0.17) and (best_tm_score < 0.9999));
      }
      */
      
      
      //////////////////////////////////////////
      // Combine matrices from angles and threading
      //////////////////////////////////////////
      /*
      fill_combined_matrix_3d(scores1, scores2);
      iteration=0;
      do
      {
        N = get_alignment(query_ali, match_ali); // this traces the DP matrix for the optimal alignment
        if (N < 4)
          break;
        prev_tm = tm;
        superpose(query_ali, match_ali, tm, rmsd);
        fill_matrix_3d();  // this generates a new DP matrix from the current superposition
        
        if (verbose and !fasta)
        {
          std::cout << "Combined refinement iteration " << iteration << ": N-aligned: " << N << ": RMSD " << rmsd << "; TM-score " << tm << "\n";
        }
        if (tm > best_tm_score)
        {
          best_query_ali = query_ali;
          best_match_ali = match_ali;
          best_tm_score = tm;
          best_rmsd = rmsd;
          best_position = match;
        }
        ++iteration;
      }
      while (refine and (iteration<10) and (tm > prev_tm) and (tm > 0.17));
      */
      
      // Move all-atom structure onto best position
      double should_be_zero = move_allatom(best_position, match_allatom);
      require(should_be_zero < 0.001, "Error moving allatom structure to desired position");
      
      // Print final alignment information
      print_aligned_sequences(best_query_ali, best_match_ali);
      if (!fasta)
      {
        //std::cout << "RMSD=" << best_rmsd << "\n";
        //std::cout << "TM-score=" << best_tm_score << "\n";
        printf("RMSD=%.3f\n", best_rmsd);
        printf("TM-score=%.3f\n", best_tm_score);
      }
      
      // Write the moved structure to disk
      if (moved_filename.size()>0)
      {
        write_to_file(match_allatom, moved_filename);
      }
      
      return best_tm_score;
    }
    
    
    // Generate an initial alignment without any 3D superposition
    // (optionally only fill the matrix, don't trace the alignment and don't
    // print anything)
    //
    double make_initial_alignment(bool doprint=true)
    {
      fill_matrix();
      if (!doprint)
        return get_score();
      std::string query_aligned, match_aligned;
      std::vector<double> equivalence;
      get_aligned_sequences(query_aligned, match_aligned, equivalence, true);
      return get_score();
    }
};

#endif
