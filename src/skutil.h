/*
     BANGalign : pseudo Bond and torsion ANGle aligner
     Copyright (C) 2009  Sebastian Kelm
 
     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SKUTIL_H
#define SKUTIL_H

#include <iostream>    // print to console
#include <fstream>     // manipulate files
#include <sstream>     // manipulate strings
#include <string>      // standard data structure
#include <vector>      // standard data structure
#include <stdexcept>   // standard exceptions
#include <typeinfo>    // investigate C++ types
#include <cstdlib>     // standard C library
#include <cctype>      // character types... isspace()


//
// CUSTOM EXCEPTIONS
//

class IllegalState : public std::runtime_error
{
  public:
    IllegalState(const std::string& s)
    : std::runtime_error(s)
    {
    }
};

class RequirementError : public std::runtime_error
{
  public:
    RequirementError(const std::string& s)
    : std::runtime_error(s)
    {
    }
};

class ValueError : public std::runtime_error
{
  public:
    ValueError(const std::string& s)
    : std::runtime_error(s)
    {
    }
};

class ParsingError : public std::runtime_error
{
  public:
    ParsingError(const std::string& s)
    : std::runtime_error(s)
    {
    }
};

class BadConversion : public ValueError
{
  public:
    BadConversion(const std::string& s)
    : ValueError(s)
    {
    }
};

class OutOfBounds : public ValueError
{
  public:
    OutOfBounds(const std::string& s)
    : ValueError(s)
    {
    }
};

class NotFound: public ValueError
{
  public:
    NotFound(const std::string& s)
    : ValueError(s)
    {
    }
};

//
// ERROR MESSAGES
//

//inline void require(bool requirement, char* errormsg = "ERROR: Requirement Failed")
//{
//  if (!requirement)
//    std::cerr << errormsg << "\n";
//}


inline void require(bool requirement, const std::string errormsg = "ERROR: Requirement Failed")
{
  if (!requirement)
    throw RequirementError(errormsg);
}




//
// STRING FUNCTIONS
//

inline bool endsWith(const std::string haystack, const std::string needle)
{
  return std::string::npos != haystack.find(needle, haystack.size()-needle.size());
}

inline bool startsWith(const std::string haystack, const std::string needle)
{
  return std::string::npos != haystack.rfind(needle, needle.size()-1);
}

// Trim whitespace from ends of a string
inline std::string trim(std::string s)
{
    using namespace std;
    
    // trim left
    size_t found = s.find_first_not_of(" \t\n\r\v\f\r");
    if (found!=string::npos)
    {
        s.erase(0,found);
    }
    
    // trim right
    found = s.find_last_not_of(" \t\n\r\v\f\r");
    if (found!=string::npos and found<s.size()-1)
    {
        s.erase(found+1);
    }
    
    if (s.size() and isspace(s.at(0)))
    {
      s.clear();
    }
    return s;
}

inline std::vector<std::string> split(const std::string& sep, const std::string& str, unsigned int maxcuts=0)
{
    unsigned int start = 0;
    unsigned int end   = 0;
    std::vector<std::string> results;
    while(end <= str.size()) // allow empty tokens at end
    {
        if (maxcuts>0 and results.size() >= maxcuts)
        {
          results.push_back(str.substr(start)); // save rest of string
          break;
        }
        end = str.find(sep, start);
        results.push_back(str.substr(start,end-start)); // save token
        start = end+sep.size(); // pop token and the next separator off the string
    }
    return results;
}


// Convert a string to a number
template<typename T>
inline T convertTo(const std::string& s)
{
    using namespace std;
    istringstream i(s);
    T x;
    if (!(i >> x))
        throw BadConversion(string("convertTo(") + typeid(x).name() + ")");
    return x;
}

template<typename T>
inline std::string toString(const std::vector<T>& v)
{
  std::stringstream ss;
  ss << "[";
  if (v.size() > 0)
  {
    typename std::vector<T>::const_iterator it = v.begin();
    for (; it<v.end()-1; it++)
    {
      ss << *it << ", ";
    }
    ss << *it;
  }
  ss << "]";
  return ss.str();
}

template<typename T>
inline std::string toString(const T& i)
{
  std::stringstream ss;
  ss << i;
  return ss.str();
}



template<typename T>
inline bool contains(const std::vector<T>& v, const T& a)
{
  if (v.size() > 0)
  {
    typename std::vector<T>::const_iterator it = v.begin();
    for (; it<v.end(); it++)
      if (*it == a)
        return 1;
  }
  return 0;
}



//
// FILENAME (STRING) MANIPULATION
//

inline std::string dirname(const std::string s)
{
  size_t found=s.find_last_of("/\\");
  return s.substr(0, found);
}
inline std::string basename(const std::string s)
{
  size_t found=s.find_last_of("/\\");
  return s.substr(found+1);
}
inline std::string remove_file_extension(const std::string s)
{
  size_t found=s.find_last_of(".");
  return s.substr(0, found);
}
inline std::string get_file_extension(const std::string s)
{
  size_t found=s.find_last_of(".");
  return s.substr(found);
}



//
// VECTOR FUNCTIONS
//

template<typename T>
inline T sum(const std::vector<T> v)
{
  T sum=0;
  for (typename std::vector<T>::const_iterator it=v.begin(); it<v.end(); it++)
  {
    sum += *it;
  }
  return sum;
}





//
// FILESYSTEM MANIPULATION
//

inline bool isFileReadable(const std::string filepath)
{
  std::ifstream in(filepath.c_str());
  if (!in)
    return false;
  in.close();
  return true;
}

inline void write_file(const std::string filename, const std::string content)
{
    std::ofstream out(filename.c_str());
    require(out, "ERROR: Could not write to file '" + filename + "'. Check permissions.");
    out << content;
    out.close();
}

inline bool test_write_file(const std::string filename)
{
    std::ofstream out(filename.c_str());
    bool success = (bool) out;
    out.close();
    remove(filename.c_str());
    return success;
}


#endif

