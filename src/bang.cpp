/*
     BANGalign : pseudo Bond and torsion ANGle aligner
     Copyright (C) 2009  Sebastian Kelm
 
     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <unistd.h>

#include "skutil.h"     // my often-used utility functions
#include "vectormath.h" // playing with 3d vectors and angles
#include "pdb.h"        // parsing PDB files
#include "bang.h"       // best-angle alignment


using namespace std;




//
// MAIN CODE EXECUTION
//

int main(int argc, char* argv[])
{
    bool quiet = 0;
    bool verbose = 0;
    bool fasta = 0;
    bool pairs = 0;
    bool no_output_files = 0;
    bool no_superpose = 0;
    bool no_refine = 0;

    double scaling_factor = 1.0; // Max angle difference (0, 1] (fraction of PI) to consider as part of a useful alignment score.

    int word_radius = 11; // -w : 1 + 2* word_radius = number of residues (bond angles) in window
    double gap_opening_score = -3.0; // -o : same meaning as in Needleman-Wunsch
    double gap_elongation_score = 0.0; // -e : same meaning as in Needleman-Wunsch

    int seq_radius = 0; // -x : 1+ 2* seq_radius = number of residues (BLOSUM scores) in window
    double seq_factor = 0.0; // -u : contribution of sequence match (BLOSUM62) toward alignment score

    int dist_radius = 6; // -y : 1+ 2* dist_radius = number of residues (contact counts) in window
    int dist_range = 17; // -a : min window around each residue to exclude local contacts
    double dist_factor = 0.7; // -z : contribution of distance match toward alignment score
    
    double gap_opening_3d = -1.5; // -n : same meaning as in Needleman-Wunsch
    double gap_extension_3d = -0.3; // -d : same meaning as in Needleman-Wunsch
    
    double D_gain_factor = 1.0;
    double D_loss_factor = 1.0;

    const string usage =
      "      ++==============================================================++\n"
      "      ||       BANGalign : pseudo Bond and torsion ANGle aligner      ||\n"
      "      ||              Copyright by Sebastian Kelm, 2009               ||\n"
      "      ++==============================================================++\n"
      "\n"
      "      USAGE: " + basename(argv[0]) + " [OPTIONS] query.pdb match.pdb [match2.pdb [...]]\n"
      "\n"
      "      BANGalign is a fast structural alignment tool. It aligns and superposes all\n"
      "      given PDB files onto the first one (unless the -p option is given).\n"
      "      Initial alignments are based on CA-CA pseudo-bond and torsion angles\n"
      "      (inspired by MAMMOTH, by Ortiz, 2002); as well as CA contact counts\n"
      "      (inspired by RAPDF, by Samudrala & Moult, 1998).\n"
      "      Superpositions optimise the TM-score, normalised by the length of the\n"
      "      shorter protein (inspired by TM-align, Zhang & Skolnick, 2005).\n"
      "\n"
      "      If more than one alignment is performed, output will be separated by '####' lines.\n"
      "\n"
      "OPTIONS:\n"
      "\n"
      "    GENERAL ALGORITHM BEHAVIOUR:\n"
      "        -p        Treat input files as pairs: query1.pdb match2.pdb [query2.pdb match2.pdb [...]]\n"
      "\n"
      "        -W        Do not write moved output PDB file.\n"
      "        -0        Stop after the super-fast initial alignment, before any 3D superposition\n"
      "                  (no TM-score or RMSD will be calculated).\n"
      "        -1        Stop after the initial superposition, before iterative re-alignment and re-superposition.\n"
      "\n"
      "        -f        Print FASTA-formatted alignments, and no scores.\n"
      "        -q        Be quiet: reduce the amount of information printed.\n"
      "        -v        Be more verbose: print alignment parameters to STDERR.\n"
      "\n"
      "        -h        Show this help message and exit.\n"
      "\n"
      "    ALIGNMENT PARAMETERS:\n"
      "      Initial alignment:\n"
      "                  Note: the angle score for a single residue ranges from -10 to +10.\n"
      "        -o -F     Gap opening penalty = F (negative float, "+toString(gap_opening_score)+")\n"
      "        -e -F     Gap elongation penalty = F (negative float, "+toString(gap_elongation_score)+")\n"
      "        -s F      Cut-off for maximum angle deviation = F (positive float, "+toString(scaling_factor)+")\n"
      "        -u W      Weight factor for sequence score contribution to the alignment score = W (positive float, "+toString(seq_factor)+")\n"
      "        -z W      Weight factor for distance score contribution to the alignment score = W (positive float, "+toString(dist_factor)+")\n"
      "        -w I      Window radius for angle    score = I (positive int, "+toString(word_radius)+")\n"
      "        -x I      Window radius for sequence score = I (positive int, "+toString(seq_radius)+")\n"
      "        -y I      Window radius for distance score = I (positive int, "+toString(dist_radius)+")\n"
      "        -a I      Min number of residues between contacting amino acids for distance score = I (positive int, "+toString(dist_range)+")\n"
      "\n"
      "      3D superposition-based refinement:\n"
      "        -n -F     Gap opening penalty for 3D superposition = F (negative float, "+toString(gap_opening_3d)+")\n"
      "        -d -F     Gap extension penalty for 3D superposition = F (negative float, "+toString(gap_extension_3d)+")\n"
      "        -g F      Distance scaling factor (times D0) for gaining residues during TM score optimisation (positive float, "+toString(D_gain_factor)+")\n"
      "        -l F      Distance scaling factor (times D0) for losing residues during TM score optimisation (positive float, "+toString(D_loss_factor)+")\n"
      "";
    // Options

    {
      // Command line option parsing

      int c;
      while (-1 != (c = getopt(argc, argv, "W01ho:e:w:x:y:z:s:u:a:n:d:g:l:qvfp")))
      {
          switch (c)
          {
            case 'h':
            {
              cerr << usage;
              return 1;
            }
            case 'W':
            {
              no_output_files = true;
              break;
            }
            case '0':
            {
              no_superpose = true;
              break;
            }
            case '1':
            {
              no_refine = true;
              break;
            }
            case 'g':
            {
              D_gain_factor = atof(optarg);
              require(D_gain_factor > 0, "ERROR: D_gain_factor must be > 0!");
              break;
            }
            case 'l':
            {
              D_loss_factor = atof(optarg);
              require(D_loss_factor > 0, "ERROR: D_loss_factor must be > 0!");
              break;
            }
            case 'n':
            {
              gap_opening_3d = atof(optarg);
              require(gap_opening_3d <= 0, "ERROR: Gap scores must be < 0!");
              break;
            }
            case 'd':
            {
              gap_extension_3d = atof(optarg);
              require(gap_extension_3d <= 0, "ERROR: Gap scores must be < 0!");
              break;
            }
            case 'o':
            {
              gap_opening_score = atof(optarg);
              require(gap_opening_score <= 0, "ERROR: Gap scores must be < 0!");
              break;
            }
            case 'e':
            {
              gap_elongation_score = atof(optarg);
              require(gap_elongation_score <= 0, "ERROR: Gap scores must be < 0!");
              break;
            }
            case 'w':
            {
              word_radius = atoi(optarg);
              require(word_radius >= 0 and word_radius <= 100, "ERROR: Angle score radius must be >= 0 and <= 100!");
              break;
            }
            case 'x':
            {
              seq_radius = atoi(optarg);
              require(seq_radius >= 0 and seq_radius <= 100, "ERROR: Seq score radius must be >= 0 and <= 100!");
              break;
            }
            case 'y':
            {
              dist_radius = atoi(optarg);
              require(dist_radius >= 0 and dist_radius <= 100, "ERROR: Dist score radius must be >= 0 and <= 100!");
              break;
            }
            case 'a':
            {
              dist_range = atoi(optarg);
              require(dist_range > 0 and dist_range <= 100, "ERROR: Dist score min residues between contacts must be > 0 and <= 100!");
              break;
            }
            case 's':
            {
              scaling_factor = atof(optarg);
              require(scaling_factor > 0 and scaling_factor <= 1, "ERROR: Scaling factor must be > 0 and <= 1!");
              break;
            }
            case 'u':
            {
              seq_factor = atof(optarg);
              require(seq_factor >= 0 and seq_factor <= 100, "ERROR: Sequence weight factor must be >= 0 and <= 100!");
              break;
            }
            case 'z':
            {
              dist_factor = atof(optarg);
              require(dist_factor >= 0 and dist_factor <= 100, "ERROR: Distance weight factor must be >= 0 and <= 100!");
              break;
            }
            case 'q':
            {
              quiet = 1;
              break;
            }
            case 'v':
            {
              verbose = 1;
              break;
            }
            case 'f':
            {
              fasta = 1;
              break;
            }
            case 'p':
            {
              pairs = 1;
              break;
            }
            default:
              abort();
          }
      }

      // End of command line option parsing
    }

    // Some restrictions on argument combinations
    //
//     require(!(optReadDensities and optWriteDensities), "ARGUMENT ERROR: Mutually exclusive options: -x, -y");

    if (optind+1 >= argc)
    {
      cerr << "ERROR: Must provide at least 2 pdb files as input.\nFor help, type\n\t" << basename(argv[0]) << " -h\n";
      return 1;
    }

    if (verbose)
    {
      cerr << "PARAMETER: [o] gap opening score:" << gap_opening_score << "\n";
      cerr << "PARAMETER: [e] gap elongation score:" << gap_elongation_score << "\n";
      cerr << "PARAMETER: [w] angle window radius:" << word_radius << "\n";
      cerr << "PARAMETER: [s] scaling factor for max angle deviation:" << scaling_factor << "\n";
      cerr << "PARAMETER: [u] sequence weight factor:" << seq_factor << "\n";
      cerr << "PARAMETER: [z] distance weight factor:" << dist_factor << "\n";
      cerr << "PARAMETER: [x] seq score window radius:" << seq_radius << "\n";
      cerr << "PARAMETER: [y] distance score window radius:" << dist_radius << "\n";
      cerr << "PARAMETER: [a] distance score residues between contacts:" << dist_range << "\n";
      cerr << "PARAMETER: [n] gap opening score for 3d superposition:" << gap_opening_3d << "\n";
      cerr << "PARAMETER: [d] gap extension score for 3d superposition:" << gap_extension_3d << "\n";
      cerr << "PARAMETER: [g] Distance scaling for gaining residues in 3d superposition:" << D_gain_factor << "\n";
      cerr << "PARAMETER: [l] Distance scaling for losing residues in 3d superposition:" << D_loss_factor << "\n";
    }


    const vector<string> atomTypes(1, "CA");

    if (!pairs)
    {
      // DATABASE SCAN (one query, many matches)
      
      string query_filename = string(argv[optind]);
      Pdb query(query_filename, &atomTypes);

      //query.print(cerr);

      vector<Coord> query_bondvectors = make_bonds(query);

      vector<double> query_bond, query_dihedral;
      make_angles(query_bondvectors, query_bond, query_dihedral);
      
      vector<Coord> query_dists = make_dists(query, dist_range);
      //string query_seq = get_pdb_seq(query);
      vector<int> query_seq = get_blosum_indices(query);

      
      //cerr << "Query bond angles : " << toString(query_bond) << "\n";
      //cerr << "Query dihedrals   : " << toString(query_dihedral) << "\n";

      for (int i=optind+1; i<argc; i++)
      {
        string match_filename = string(argv[i]);

        if (!isFileReadable(match_filename))
        {
          cerr << "ERROR: File not readable. Skipping: '" << match_filename << "'\n";
          continue;
        }

        Pdb match_allatom(match_filename);
        Pdb match(match_allatom, &atomTypes);
        string match_moved_name;
        if (!no_output_files)
        {
          match_moved_name = remove_file_extension(match_filename) + ".bang.pdb";
        }

        //match.print(cerr);

        vector<Coord> match_bondvectors = make_bonds(match);

        vector<double> match_bond, match_dihedral;
        make_angles(match_bondvectors, match_bond, match_dihedral);

        //cerr << "Match bond angles : " << toString(match_bond) << "\n";
        //cerr << "Match dihedrals   : " << toString(match_dihedral) << "\n";

        //if (!quiet or (argc-optind > 2)) // more than one alignment to do in total
        //{
        //  cout << "Query file=" << query_filename << "\n";
        //  cout << "Match file=" << match_filename << "\n";
        //}


        //////////////
        Aligner ali(query, match, query_dists, make_dists(match, dist_range), query_seq, get_blosum_indices(match), query_bond, query_dihedral, match_bond, match_dihedral, word_radius, gap_opening_score, gap_elongation_score, scaling_factor, seq_factor, seq_radius, dist_factor, dist_radius, gap_opening_3d, gap_extension_3d, D_gain_factor, D_loss_factor);
        ali.set_output_options(!quiet, fasta);
        if (no_superpose)
          ali.make_initial_alignment(true);
        else
          ali.superpose(match_allatom, match_moved_name, !no_refine);
        //////////////
        
        if (i<argc-1)
        {
          cout << "####\n";
        }
      }
    }
    else
    {
      // PAIRS (one query, one match)
      for (int i=optind+1; i<argc; i+=2)
      {
        string query_filename = string(argv[i-1]);
        string match_filename = string(argv[i]);
        if (!isFileReadable(query_filename))
        {
          cerr << "ERROR: File not readable. Skipping: '" << query_filename << "'\n";
          continue;
        }
        if (!isFileReadable(match_filename))
        {
          cerr << "ERROR: File not readable. Skipping: '" << match_filename << "'\n";
          continue;
        }
        Pdb query(query_filename, &atomTypes);
        Pdb match_allatom(match_filename);
        Pdb match(match_allatom, &atomTypes);
        string match_moved_name;
        if (!no_output_files)
        {
          match_moved_name = remove_file_extension(match_filename) + ".bang.pdb";
        }

        vector<Coord> query_bondvectors = make_bonds(query);
        vector<Coord> match_bondvectors = make_bonds(match);

        vector<double> query_bond, query_dihedral, match_bond, match_dihedral;
        make_angles(query_bondvectors, query_bond, query_dihedral);
        make_angles(match_bondvectors, match_bond, match_dihedral);
        
        
        //////////////
        Aligner ali(query, match, make_dists(query, dist_range), make_dists(match, dist_range), get_blosum_indices(query), get_blosum_indices(match), query_bond, query_dihedral, match_bond, match_dihedral, word_radius, gap_opening_score, gap_elongation_score, scaling_factor, seq_factor, seq_radius, dist_factor, dist_radius, gap_opening_3d, gap_extension_3d, D_gain_factor, D_loss_factor);
        ali.set_output_options(!quiet, fasta);
        if (no_superpose)
          ali.make_initial_alignment(true);
        else
          ali.superpose(match_allatom, match_moved_name, !no_refine);
        //////////////

        if (i<argc-1)
        {
          cout << "####\n";
        }
      }
    }

    return 0; // the end
}
