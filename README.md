      ++==============================================================++
      ||       BANGalign : pseudo Bond and torsion ANGle aligner      ||
      ||              Copyright by Sebastian Kelm, 2009               ||
      ++==============================================================++

      BANGalign is released under the GNU General Public License v3.
      For details, see the included LICENSE file.

      USAGE: bang [OPTIONS] query.pdb match.pdb [match2.pdb [...]]

      BANGalign is a fast structural alignment tool. It aligns and superposes all
      given PDB files onto the first one (unless the -p option is given).
      Initial alignments are based on CA-CA pseudo-bond and torsion angles
      (inspired by MAMMOTH, by Ortiz, 2002); as well as CA contact counts
      (inspired by RAPDF, by Samudrala & Moult, 1998).
      Superpositions optimise the TM-score, normalised by the length of the
      shorter protein (inspired by TM-align, Zhang & Skolnick, 2005).

      If more than one alignment is performed, output will be separated by '####' lines.

OPTIONS:

    GENERAL ALGORITHM BEHAVIOUR:
        -p        Treat input files as pairs: query1.pdb match2.pdb [query2.pdb match2.pdb [...]]

        -W        Do not write moved output PDB file.
        -0        Stop after the super-fast initial alignment, before any 3D superposition
                  (no TM-score or RMSD will be calculated).
        -1        Stop after the initial superposition, before iterative re-alignment and re-superposition.

        -f        Print FASTA-formatted alignments, and no scores.
        -q        Be quiet: reduce the amount of information printed.
        -v        Be more verbose: print alignment parameters to STDERR.

        -h        Show this help message and exit.

    ALIGNMENT PARAMETERS:
      Initial alignment:
                  Note: the angle score for a single residue ranges from -10 to +10.
        -o -F     Gap opening penalty = F (negative float, -10)
        -e -F     Gap elongation penalty = F (negative float, -2)
        -s F      Cut-off for maximum angle deviation = F (positive float, 1)
        -u W      Weight factor for sequence score contribution to the alignment score = W (positive float, 0)
        -z W      Weight factor for distance score contribution to the alignment score = W (positive float, 0.7)
        -w I      Window radius for angle    score = I (positive int, 11)
        -x I      Window radius for sequence score = I (positive int, 0)
        -y I      Window radius for distance score = I (positive int, 6)
        -a I      Min number of residues between contacting amino acids for distance score = I (positive int, 17)

      3D superposition-based refinement:
        -n -F     Gap opening penalty for 3D superposition = F (negative float, -2.8)
        -d -F     Gap extension penalty for 3D superposition = F (negative float, -0.2)

